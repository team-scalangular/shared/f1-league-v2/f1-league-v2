import Mock = jest.Mock;

export const createSpyObj = (baseName: any, methodNames: any): { [key: string]: Mock<any> } => {
  const obj: any = {};

  for (let i = 0; i < methodNames.length; i++) {
    obj[methodNames[i]] = jest.fn();
  }

  return obj;
};
