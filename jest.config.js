const { getJestProjects } = require('@nrwl/jest');

module.exports = {
  setupFile: ['helper.ts'],
  projects: [...getJestProjects(), '<rootDir>/libs/f1-league-v2/home'],
};
