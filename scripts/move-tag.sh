#!/usr/bin/env bash
# script to move a local tag to a specific commit
# for example after a rebase happened
# so that the semi linear history is not broken
# USAGE: "scripts/move-tag.sh -t <tagName> -h <commitHash>"

while getopts t:h: flag
do
    case "${flag}" in
        t) tagName=${OPTARG};;
        h) hash=${OPTARG};;
        *) echo "unknown flags provided"
          exit 1;;
    esac
done

if [ -z "$tagName" ] || [ -z "$hash" ]
then
  echo "please provide tagName (-t) and commitHash (-h)"
  exit 1;
fi

git tag -d "$tagName"
git push origin :refs/tags/"$tagName"
git tag "$tagName" "$hash"
git push origin "$tagName"

