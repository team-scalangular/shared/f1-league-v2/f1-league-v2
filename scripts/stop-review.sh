#!/usr/bin/env bash

export STACK_NAME="$PROJECT_NAME-$CI_COMMIT_REF_SLUG"
STACK_NAME="${STACK_NAME:0:31}"

# Get API Token
json=$(curl --location --request POST 'portainer.scalangular.com/api/auth' \
--header 'Content-Type: application/json' \
--data-raw '{"Username": "'$PORTAINER_USER'", "Password": "'"$PORTAINER_PASSWORD"'"}') \
&& token=$(echo $json | sed "s/{.*\"jwt\":\"\([^\"]*\).*}/\1/g") \

#try to get the stack id
json=$(curl --location --request GET 'portainer.scalangular.com/api/stacks' \
--header "Authorization: Bearer $token")

stackId=$(echo "$json" | jq '.[] | select(.Name=="'$STACK_NAME'") | .Id')

if [ -z "$stackId" ]
then
  echo "Something went wrong while locating the stackId. Please delete it manually!"
  exit 1;
else
  curl --location --request DELETE "portainer.scalangular.com/api/stacks/$stackId" \
  --header "Authorization: Bearer $token"
  exit 0;
fi
