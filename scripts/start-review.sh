#!/usr/bin/env bash

# Get API Token
json=$(curl --location --request POST 'portainer.scalangular.com/api/auth' \
--header 'Content-Type: application/json' \
--data-raw '{"Username": "'$PORTAINER_USER'", "Password": "'"$PORTAINER_PASSWORD"'"}') \
&& token=$(echo $json | sed "s/{.*\"jwt\":\"\([^\"]*\).*}/\1/g") \

echo "Commit slug: "
echo "$CI_COMMIT_REF_SLUG"

# Get compose yaml configurations
export IMAGE_NAME="$PROJECT_NAME-$CI_COMMIT_REF_SLUG"
export STACK_NAME="${IMAGE_NAME:0:31}"
export DOMAIN_NAME="\`$CI_ENVIRONMENT_SLUG.scalangular.com\`"

( echo "cat <<EOF >final.yaml";
  cat ./scripts/helpers/compose-template.yaml;
  echo "EOF";
) >temp.yaml
. temp.yaml
cat final.yaml

echo "Compose YAML is created. Deploying the stack with the previous configurations"
sleep 5s

#check if the stack already exists
json=$(curl --location --request GET 'portainer.scalangular.com/api/stacks' \
--header "Authorization: Bearer $token")

stackId=$(echo "$json" | jq '.[] | select(.Name=="'$STACK_NAME'") | .Id')

if [ -z "$stackId" ]
then
  echo "No stack deployed: creating a new one."
else
  echo "Review-Stack already deployed to stackId: $stackId"
  echo "Stack will be deleted before creating a new one"
  curl --location --request DELETE "portainer.scalangular.com/api/stacks/$stackId" \
  --header "Authorization: Bearer $token"
  sleep 10s
fi

#deploy the stack
curl --location --request POST 'portainer.scalangular.com/api/stacks?type=1&method=file&endpointId=1' \
--header "Authorization: Bearer $token" \
--form "Name=$STACK_NAME" \
--form "EndpointID='1'" \
--form "SwarmID=$REVIEW_SWARM" \
--form 'file=@"./final.yaml"' | jq .

sleep 5s

#verify deployment
json=$(curl --location --request GET 'portainer.scalangular.com/api/stacks' \
--header "Authorization: Bearer $token")

stackId=$(echo "$json" | jq '.[] | select(.Name=="'$STACK_NAME'") | .Id')

if [ -z "$stackId" ]
then
  echo "Something went wrong deploying the stack."
  exit 1;
else
  echo "Review-Stack deployed to stackId: $stackId"
  exit 0;
fi
