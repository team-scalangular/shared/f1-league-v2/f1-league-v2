# F1LeagueV2

This project was generated using [Nx](https://nx.dev).
It follows the common structure of NX Applications.


The project represents the frontend for the F1 League (V2) application


The application consists of the frontend (current project) and a Kotlin Backend with an SQL Database. <br>
You can view the other project from the GitLab Group F1 League V2.

## Environments

- Production: https://f1.scalangular.com
- Beta: https://beta.f1.scalangular.com

## Get started

### Prerequisites

To get the project run locally you will need to Install Node.js on your system. (LTS Recommendet) <br>
You will also need a Version of Java available on the system (for the code generation)


Besides that you will also need to globally install the open api generator cli via npm. <br>
This will make you able to generate the needed http service classes.

```shell 
npm i -g @openapitools/openapi-generator-cli
```

Also it will be useful, to have a global version of the angular/cli and the nrwl/cli 

```shell 
npm i -g @angular/cli
npm i -g @nrwl/cli
```


### Clone the repo

```shell
git clone git@gitlab.com:team-scalangular/shared/f1-league-v2/f1-league-v2.git
cd f1-league-v2
```

### Install npm packages

Install the `npm` packages described in the `package.json`:

```shell
npm install
```

Verify that everything is working as expected by running 

```shell
npm run start
```

This should generate the service classes into -> ./generated <br>
As well as serve the angular project to localhost:4200

#### npm scripts

These are the most useful commands defined in `package.json`:

* `npm start` - generates the api and serves the project to localhost:4200
* `npm run build` - builds the application (staging or production flags are possible).
* `npm run format:check` - prettier check (if errors use npm run format:write)

For the rest use the affected scripts to only test files which have been modified.

e.g.
* `npm run affected:test` - unit test affected libs and apps
* `npm run affected:lint` - eslint for affected libs and apps

scripts with the suffix -ci have the purpose to work inside the gitlab ci. <br>
you don't need to use them, but it should work. <br>
All the scripts are listed in the package.json file.

### Build and Deployment (CI/CD)

The project uses the GitLab Ci/CD Features. To view the Stages and Configurations 
you can look inside the GitLab Ci Yaml. <br>

#### Continues Integration

For the CI Part the project is tested in 3 Stages including: 
- Package installation (npm + cypress and openAPI generation)
- Linting (Eslint + Prettier Check)
- Test and Build - Unit Tests (Jest) and ng build

#### Continues Deployment

The app is deployed to an Portainer Docker Swarm cluster.
We use deplyoment via git tag for: 
- Release (Prod) - Tag: Release_x.x.x
- Beta (Staging) - Tag: Beta_x.x.x

For the deployment a docker image is created which is pushed to the Gitlab registry.
Afterwards the Portainer server gets notified via curl Request, to update its stack.

There are multiple Dockerfiles and nginx.conf files for the different stages.
You can locally test the deployment, if you have docker installed on your machine. <br>

To build the docker image use:

- Release: ```docker build . -t "release-image"```
- Beta: ```docker build -f Dockerfile.beta . -t "beta-image"```

To run them:

- Release: ```docker run -d -p 8080:80 "release-image"```
- Beta: ```docker run -d -p 8080:80 beta-image``` 

-> Runs Images on localhost:8080 <br>
-> For the beta image you will also need to make your local .htpasswd file
