import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EntryComponent } from '@libs/home';

const routes: Routes = [
  {
    path: '',
    component: EntryComponent,
  },
  {
    path: 'league-view',
    loadChildren: () => import('@libs/league-view').then((m) => m.LeagueViewModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
