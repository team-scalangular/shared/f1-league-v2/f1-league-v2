import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RippleModule } from 'primeng/ripple';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderModule } from '@libs/header';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  apiModuleBy,
  defaultStoreConfig,
  devtoolsConfigBy,
  entityDataModuleConfig,
  globalEffects,
  globalReducers,
} from '@libs/utils';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { EntityDataModule } from '@ngrx/data';
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RippleModule,
    HeaderModule,
    StoreModule.forRoot(globalReducers, defaultStoreConfig),
    EffectsModule.forRoot(globalEffects),
    devtoolsConfigBy(environment),
    apiModuleBy(environment),
    EntityDataModule.forRoot(entityDataModuleConfig),
    HttpClientModule,
    ToastModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
