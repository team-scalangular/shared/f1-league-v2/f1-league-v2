import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'f1-league-v2-home-entry',
  template: `
    <div class="under-construction">
      <div class="under-construction__container">
        <h1 class="under-construction__heading">Under Construction</h1>
        <hr />
        <h3 class="under-construction__text">
          Please click on the
          <a routerLink="league-view" class="under-construction__link">"View Leagues"</a> tab to see
          the progress of the current F1 League <br />
          More features will be implemented later on.
        </h3>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntryComponent {}
