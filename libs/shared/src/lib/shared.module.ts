import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';
import { LeagueFieldConverterPipe } from './pipes/league-field-converter.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [LoaderComponent, LeagueFieldConverterPipe],
  exports: [LoaderComponent, LeagueFieldConverterPipe],
})
export class SharedModule {}
