import { Pipe, PipeTransform } from '@angular/core';
import { toReadableTime } from '@libs/utils';
import * as dayjs from 'dayjs';

@Pipe({
  name: 'leagueFieldConverter',
})
export class LeagueFieldConverterPipe implements PipeTransform {
  transform(value: string | number, id: string): string {
    if (id === 'fastestLap' || id === 'totalTime' || id === 'raceTime') {
      if (!isNaN(value as number)) {
        return toReadableTime(value as number);
      } else {
        return value as string;
      }
    }
    if (id === 'date' && value !== 'TBD.') {
      return dayjs(value as string).format('DD.MM.YYYY');
    }
    return value as string;
  }
}
