import { LeagueFieldConverterPipe } from '../league-field-converter.pipe';

describe('LeagueFieldConverter Pipe', () => {
  const pipe = new LeagueFieldConverterPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return a lap time for fastestLap', () => {
    const value = 103.38025;
    const expected = '01:43:380';

    const result = pipe.transform(value, 'fastestLap');

    expect(result).toBe(expected);
  });

  test.each([['totalTime'], ['raceTime']])(
    'should return a converted raceTime for %s',
    (mockId) => {
      const value = 1390.8109130859375;
      const expected = '23:10:811';

      const result = pipe.transform(value, mockId);

      expect(result).toBe(expected);
    }
  );

  it('should format the date correctly or return TBD. for no date', () => {
    const value = '2021-05-11';
    const valueTbd = 'TBD.';

    const expected = '11.05.2021';

    const result = pipe.transform(value, 'date');
    const resultTbd = pipe.transform(valueTbd, 'date');

    expect(result).toBe(expected);
    expect(resultTbd).toBe(valueTbd);
  });
});
