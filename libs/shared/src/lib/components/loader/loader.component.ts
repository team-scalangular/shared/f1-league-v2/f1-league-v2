import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'shared-loader',
  template: `
    <div class="sh-loader">
      <span class="loader"></span>
    </div>
  `,
  styleUrls: ['./loader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoaderComponent {}
