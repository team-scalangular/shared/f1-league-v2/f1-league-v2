export const enum LoadingState {
  INIT = 'INIT',
  LOADING = 'LOADING',
  LOADED = 'LOADED',
}

export interface ErrorState {
  errorMessage: string;
}

export type CallState = LoadingState | ErrorState;

export const extractErrorIfPresent = (callState: CallState): string | null =>
  (callState as ErrorState).errorMessage !== undefined
    ? (callState as ErrorState).errorMessage
    : null;

export const isLoading = (callState: CallState) => callState === LoadingState.LOADING;

export const isLoaded = (callState: CallState) => callState === LoadingState.LOADED;
