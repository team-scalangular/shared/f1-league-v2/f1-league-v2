import { Action, ActionReducerMap, MetaReducer, RootStoreConfig, RuntimeChecks } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ModuleWithProviders, Type } from '@angular/core';
import { DefaultEnvironment } from '../interfaces/default-env';
import {
  EntityDataModuleConfig,
  EntityDispatcherDefaultOptions,
  EntityMetadataMap,
} from '@ngrx/data';
import { environment } from '@f1-league-v2-core/src/environments/environment';

export const metaReducersBy = (
  env: DefaultEnvironment
): MetaReducer<Record<string, never>, Action>[] => (!env.production ? [] : []);

export const runtimeChecks: Partial<RuntimeChecks> = {
  strictActionImmutability: true,
  strictStateImmutability: true,
};

export function devtoolsConfigBy(
  env: DefaultEnvironment
): ModuleWithProviders<StoreDevtoolsModule> | [] {
  return !env.production ? StoreDevtoolsModule.instrument() : [];
}

export const defaultEntityDispatcherOptions: Partial<EntityDispatcherDefaultOptions> = {
  optimisticAdd: false,
  optimisticUpsert: true,
  optimisticUpdate: true,
  optimisticDelete: true,
};

export const defaultStoreConfig: RootStoreConfig<unknown> = {
  metaReducers: metaReducersBy(environment),
  runtimeChecks,
};

export const globalReducers: ActionReducerMap<unknown> = {};
export const globalEffects: Type<unknown>[] | undefined = [];

export const STANDINGS_ENTITY_KEY = 'Standings';
export const RACE_RESULTS_ENTITY_KEY = 'RaceResults';

export const entityMetadataMap: EntityMetadataMap = {
  [STANDINGS_ENTITY_KEY]: {
    selectId: (model) => model.driverId,
    entityDispatcherOptions: defaultEntityDispatcherOptions,
    sortComparer: (a, b) => b.points - a.points,
  },
  [RACE_RESULTS_ENTITY_KEY]: {
    selectId: (model) => model.raceId,
    entityDispatcherOptions: defaultEntityDispatcherOptions,
  },
};

export const entityDataModuleConfig: EntityDataModuleConfig = { entityMetadata: entityMetadataMap };
