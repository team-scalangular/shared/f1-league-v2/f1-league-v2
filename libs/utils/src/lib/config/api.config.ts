import { ApiModule, Configuration } from '@generated/openapi';
import { ModuleWithProviders } from '@angular/core';
import { DefaultEnvironment } from '../interfaces/default-env';

export function apiModuleBy(
  environment: DefaultEnvironment
): ApiModule[] | ModuleWithProviders<ApiModule> {
  return environment.production ? [ApiModule] : ApiModule.forRoot(betaConfig);
}

export const betaConfig = (): Configuration =>
  new Configuration({
    basePath: 'https://api.v2.beta.f1league.scalangular.com',
  });
