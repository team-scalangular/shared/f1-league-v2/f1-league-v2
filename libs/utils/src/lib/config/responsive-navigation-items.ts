import { MenuItem } from 'primeng/api';

export const responsiveNavigationItems: MenuItem[] = [
  {
    label: 'Dashboards',
    items: [
      {
        label: 'Home',
        icon: 'pi pi-home',
        routerLink: '/',
      },
      {
        label: 'League View',
        icon: 'pi pi-desktop',
        routerLink: 'league-view',
      },
    ],
  },
];
