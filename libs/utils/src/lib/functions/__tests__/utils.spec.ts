import { toReadableTime } from '@libs/utils';

describe('Utils', () => {
  describe('to readable time', () => {
    const lapTimeInSeconds = 103.38025;
    const raceTimeInSeconds = 1390.8109130859375;

    test.each([
      [lapTimeInSeconds, '01:43:380'],
      [raceTimeInSeconds, '23:10:811'],
    ])('testing %s with expected result %s', (timeMock: number, expected: string) => {
      const result = toReadableTime(timeMock);

      expect(result).toBe(expected);
    });
  });
});
