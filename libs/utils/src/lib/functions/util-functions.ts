export const notEmpty = <T>(arg: T | null | undefined): arg is T =>
  arg !== null && arg !== undefined;

export const toSingleLoadingFlag = (flags: boolean[]): boolean =>
  flags.some((isLoading: boolean) => isLoading);

export const toLoadedFlag = (flags: boolean[]): boolean => flags.every(Boolean);

// SOURCE: https://gist.github.com/vankasteelj/74ab7793133f4b257ea3
export const toReadableTime = (timeInSeconds: number): string => {
  const pad = function (num: number, size: number) {
    return ('000' + num).slice(size * -1);
  };
  const time = timeInSeconds.toFixed(3);
  const minutes = Math.floor(+time / 60) % 60;
  const seconds = Math.floor(+time - minutes * 60);
  const milliseconds = time.slice(-3);

  return pad(minutes, 2) + ':' + pad(seconds, 2) + ':' + pad(+milliseconds, 3);
};
