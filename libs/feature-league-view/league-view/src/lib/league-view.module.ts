import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EntryComponent } from './entry/entry.component';
import { FeatureLeagueViewUiModule } from '@libs/league-view/ui';
import { FeatureLeagueViewDataAccessModule } from '@libs/league-view-data-access';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: EntryComponent }]),
    FeatureLeagueViewUiModule,
    FeatureLeagueViewDataAccessModule,
  ],
  declarations: [EntryComponent],
})
export class LeagueViewModule {}
