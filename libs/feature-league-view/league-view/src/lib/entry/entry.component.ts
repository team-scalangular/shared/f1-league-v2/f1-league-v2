import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  template: `
    <section>
      <f1-league-v2-league-subheader></f1-league-v2-league-subheader>
      <f1-league-v2-league-view></f1-league-v2-league-view>
    </section>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntryComponent {}
