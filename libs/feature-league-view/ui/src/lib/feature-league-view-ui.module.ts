import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeagueSubheaderComponent } from './league-subheader/league-subheader.component';
import { LeagueViewComponent } from './league-view/league-view.component';
import { LeagueViewTableComponent } from './league-view-table/league-view-table.component';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { SkeletonModule } from 'primeng/skeleton';
import { SharedModule } from '@libs/shared';
import { FilterSelectComponent } from './filter-select/filter-select.component';
import { CascadeSelectModule } from 'primeng/cascadeselect';
import { FormsModule } from '@angular/forms';
import { LeagueViewTableSkeletonComponent } from './league-view-table/league-view-table-skeleton/league-view-table-skeleton.component';
import { ButtonModule } from 'primeng/button';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    CardModule,
    SkeletonModule,
    SharedModule,
    CascadeSelectModule,
    FormsModule,
    ButtonModule,
  ],
  declarations: [
    LeagueSubheaderComponent,
    LeagueViewComponent,
    LeagueViewTableComponent,
    FilterSelectComponent,
    LeagueViewTableSkeletonComponent,
  ],
  exports: [LeagueSubheaderComponent, LeagueViewComponent],
})
export class FeatureLeagueViewUiModule {}
