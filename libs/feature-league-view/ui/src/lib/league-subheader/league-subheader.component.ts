import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { FilterSelectData, showTableHeading } from '@league-view/utils';
import { LeagueFacade } from '@libs/league-view-data-access';

@Component({
  selector: 'f1-league-v2-league-subheader',
  template: `
    <div class="container-fluid">
      <div class="row subheader__row">
        <div class="col-4 mr-auto">
          <div class="subheader__title-wrapper">
            <div class="subheader__title">
              <h1>F1 Event League</h1>
            </div>
          </div>
        </div>
        <div class="col-4" *ngIf="filterData$ | async as filterState">
          <div class="subheader__info-wrapper text-center">
            <h4>
              {{
                _showTableHeading(
                  filterState.activeDrFilter,
                  filterState.activeEntityFilter,
                  filterState.driversToDisplay,
                  filterState.racesToDisplay
                )
              }}
            </h4>
          </div>
        </div>
        <div class="col-4 ml-auto">
          <div class="subheader__next-race-wrapper">
            <div class="subheader__next-race">--Next Race Here--</div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./league-subheader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LeagueSubheaderComponent {
  filterData$: Observable<FilterSelectData> = this.leagueFacade.filterSelectData$;
  _showTableHeading = showTableHeading;

  constructor(private leagueFacade: LeagueFacade) {}
}
