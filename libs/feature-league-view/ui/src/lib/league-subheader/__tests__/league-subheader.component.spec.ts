import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LeagueSubheaderComponent } from '../league-subheader.component';
import { LeagueFacade } from '@libs/league-view-data-access';
import { createSpyObj } from '../../../../../../../helper';

describe('LeagueSubheaderComponent', () => {
  let component: LeagueSubheaderComponent;
  let fixture: ComponentFixture<LeagueSubheaderComponent>;
  const leagueFacadeMock = createSpyObj(LeagueFacade, []);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LeagueSubheaderComponent],
      providers: [{ provide: LeagueFacade, useValue: leagueFacadeMock }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeagueSubheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should match the snapshot', () => {
    expect(fixture).toMatchSnapshot();
  });
});
