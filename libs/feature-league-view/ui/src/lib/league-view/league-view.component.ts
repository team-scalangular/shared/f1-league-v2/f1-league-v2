import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { LeagueActions } from '../../../../data-access/src/lib/state/league/league-view-types';
import {
  LeagueFacade,
  PdfExportService,
  RouteMetadataProvider,
} from '@libs/league-view-data-access';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FilterSelectData, showTableHeading } from '@league-view/utils';

@UntilDestroy()
@Component({
  selector: 'f1-league-v2-league-view',
  template: `
    <div class="container-fluid" *ngIf="filterData$ | async as filterState">
      <div class="row">
        <div class="col-12">
          <f1-league-v2-league-view-filter-select [filterSelectData]="filterState">
          </f1-league-v2-league-view-filter-select>
        </div>
      </div>
    </div>
    <div class="container-fluid" id="anchor">
      <div class="row">
        <div class="col-12">
          <f1-league-v2-league-view-table></f1-league-v2-league-view-table>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./league-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LeagueViewComponent implements OnInit {
  filterData$: Observable<FilterSelectData> = this.leagueFacade.filterSelectData$;
  _showTableHeading = showTableHeading;

  constructor(
    private store: Store,
    private leagueFacade: LeagueFacade,
    private routeMetadataProvider: RouteMetadataProvider,
    private route: ActivatedRoute,
    private pdfExportService: PdfExportService
  ) {}

  ngOnInit(): void {
    this.store.dispatch(
      LeagueActions.leagueSelected({
        leagueId: '4ccc5a4f-bdcb-4bd5-a7e8-2382adf804e2',
      })
    );
    this.initializeNeededObservables();
  }

  private initializeNeededObservables(): void {
    this.routeMetadataProvider.queryParamsToFilter$.pipe(untilDestroyed(this)).subscribe();
    this.pdfExportService.downloadHandler$.pipe(untilDestroyed(this)).subscribe();
  }
}
