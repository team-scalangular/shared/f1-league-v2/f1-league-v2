import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';
import {
  LeagueFacade,
  PdfExportService,
  RouteMetadataProvider,
} from '@libs/league-view-data-access';
import {
  createFilterConfig,
  createSelectedFilter,
  DRFilters_enum,
  FilterConfig,
  FilterSelectData,
  FilterSelectOptions,
} from '@league-view/utils';

@Component({
  selector: 'f1-league-v2-league-view-filter-select',
  template: `
    <div class="filter-select__wrapper">
      <h5>Selected Filter:</h5>
      <div class="filter-select__inputs">
        <p-cascadeSelect
          [(ngModel)]="selectedFilter"
          [options]="filterOptions"
          optionLabel="fName"
          optionGroupLabel="name"
          [optionGroupChildren]="['childs']"
          [style]="{ minWidth: '14rem' }"
          placeholder="Select a Filter"
          (ngModelChange)="updateFilter($event)"
        ></p-cascadeSelect>
        <button
          *ngIf="
            filterSelectData?.activeDrFilter !== drFiltersEnum.FILTER_DRIVERS &&
            filterSelectData?.activeEntityFilter !== ''
          "
          pButton
          type="button"
          icon="pi pi-cloud-download"
          class="ml-auto p-button-raised p-button-text p-button-plain filter-select__download"
          (click)="dispatchNextDownload()"
        ></button>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterSelectComponent implements OnChanges {
  @Input() filterSelectData: FilterSelectData | undefined = undefined;

  filterOptions: FilterConfig[] = [];
  selectedFilter: FilterSelectOptions = { fName: '', fCode: '' };
  drFiltersEnum = DRFilters_enum;

  constructor(
    private leagueFacade: LeagueFacade,
    private routeMetadataProvider: RouteMetadataProvider,
    private pdfExportService: PdfExportService
  ) {}

  ngOnChanges(): void {
    if (this.filterSelectData) {
      this.filterOptions = createFilterConfig(
        this.filterSelectData.driversToDisplay,
        this.filterSelectData.racesToDisplay
      );

      this.selectedFilter = createSelectedFilter(
        this.filterSelectData.activeDrFilter,
        this.filterSelectData.activeEntityFilter,
        this.filterSelectData.driversToDisplay,
        this.filterSelectData.racesToDisplay
      );
    }
  }

  updateFilter(evt: FilterSelectOptions): void {
    if (evt.fName.startsWith('Driver')) {
      this.routeMetadataProvider.updateRoute(DRFilters_enum.FILTER_DRIVERS, evt.fCode);
    } else {
      this.routeMetadataProvider.updateRoute(DRFilters_enum.FILTER_RACES, evt.fCode);
    }
  }

  dispatchNextDownload(): void {
    this.pdfExportService.downloadRaceExport$.next(this.selectedFilter.fCode);
  }
}
