import { LeagueData, TableData } from '@league-view/utils';

export interface LeagueViewTableState {
  tableData: TableData;
  leagueData: LeagueData;
  loading: boolean;
}
