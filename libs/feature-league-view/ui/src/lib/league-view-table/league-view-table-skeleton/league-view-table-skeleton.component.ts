import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'f1-league-v2-league-view-table-skeleton',
  template: `
    <p-table [value]="initialEmpty">
      <ng-template pTemplate="header">
        <tr>
          <th>Code</th>
          <th>Name</th>
          <th>Category</th>
          <th>Quantity</th>
        </tr>
      </ng-template>
      <ng-template pTemplate="body" let-product>
        <tr>
          <td><p-skeleton></p-skeleton></td>
          <td><p-skeleton></p-skeleton></td>
          <td><p-skeleton></p-skeleton></td>
          <td><p-skeleton></p-skeleton></td>
        </tr>
      </ng-template>
    </p-table>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LeagueViewTableSkeletonComponent {
  initialEmpty = Array(getSkeletonRowCount()).map(() => ({}));
}

const getSkeletonRowCount = (): number => {
  /*eslint-disable */
  return Math.round(
    (document.documentElement.clientHeight - document.getElementById('anchor')!.offsetTop - 100) /
      30
  );
};
