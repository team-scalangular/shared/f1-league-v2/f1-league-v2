import { ChangeDetectionStrategy, Component, HostListener } from '@angular/core';
import { LeagueFacade } from '@libs/league-view-data-access';
import { Observable } from 'rxjs';
import { createComponentState, LeagueData, TableData } from '@league-view/utils';
import { LeagueViewTableState } from './league-view-table';
import { Maybe } from 'monet';

@Component({
  selector: 'f1-league-v2-league-view-table',
  template: `
    <div *ngIf="(loading$ | async) === false; else showSkeleton">
      <p-table
        *ngIf="$any(state$ | async); let state"
        [value]="state.leagueData"
        [scrollable]="true"
        [scrollHeight]="calculateScrollHeight()"
        styleClass="p-datatable-striped"
      >
        <ng-template pTemplate="header">
          <tr>
            <th *ngFor="let col of state.tableData">
              {{ col.header }}
            </th>
          </tr>
        </ng-template>
        <ng-template pTemplate="body" let-rowData>
          <tr>
            <td
              *ngFor="let col of state.tableData"
              [ngClass]="{ 'fastest-lap': isFastestLap(col.field, rowData) }"
            >
              {{ rowData[col.field] | leagueFieldConverter: col.field }}
            </td>
          </tr>
        </ng-template>
      </p-table>
    </div>
    <ng-template #showSkeleton>
      <f1-league-v2-league-view-table-skeleton></f1-league-v2-league-view-table-skeleton>
    </ng-template>
  `,
  styleUrls: ['./league-view-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LeagueViewTableComponent {
  tableData$: Observable<TableData> = this.leagueFacade.tableData$;
  leagueData$: Observable<LeagueData> = this.leagueFacade.leagueData$;
  loading$: Observable<boolean> = this.leagueFacade.loadingLeagueView$;
  state$: Observable<LeagueViewTableState> = createComponentState(
    this.tableData$,
    this.leagueData$,
    this.loading$
  );

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.calculateScrollHeight();
  }

  constructor(private leagueFacade: LeagueFacade) {}

  // Row Data is typeof TableData
  isFastestLap(field: string, rowData: { isFastestLap?: boolean }): boolean {
    return Maybe.fromNull(rowData.isFastestLap).cata(
      () => false,
      (isFastest: boolean) => (field === 'fastestLap' ? isFastest : false)
    );
  }

  calculateScrollHeight(): string {
    /*eslint-disable */
    return `${
      document.documentElement.clientHeight - document.getElementById('anchor')!.offsetTop - 20
    }px`;
  }
}
