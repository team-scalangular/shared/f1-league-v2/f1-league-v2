import { Injectable } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';
import { LeagueFacade } from '../state/league/league.facade';
import { DRFilters_enum } from '@league-view/utils';

@Injectable()
export class RouteMetadataProvider {
  queryParamsToFilter$: Observable<void> = this.route.queryParamMap.pipe(
    tap(this.updateFilter.bind(this)),
    mergeMap(() => of(void 0))
  );

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private leagueFacade: LeagueFacade
  ) {}

  private updateFilter(paramsMap: ParamMap): void {
    if (paramsMap.keys.length === 0) {
      this.leagueFacade.updateDriverFilter(DRFilters_enum.FILTER_DRIVERS);
    } else if (paramsMap.get('filter') === DRFilters_enum.FILTER_DRIVERS) {
      this.leagueFacade.updateDriverFilter(DRFilters_enum.FILTER_DRIVERS);
    } else {
      this.leagueFacade.updateDriverFilter(DRFilters_enum.FILTER_RACES);
    }
    this.leagueFacade.updateEntityFilter(paramsMap.get('id') || '');
  }

  public updateRoute(drFilter: string, etFilter: string): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { filter: drFilter, id: etFilter },
      queryParamsHandling: 'merge',
    });
  }
}
