import { Injectable } from '@angular/core';
import { DefaultService } from '@generated/openapi';
import { Observable, of, Subject } from 'rxjs';
import { selectLeagueId } from '@libs/league-view-data-access';
import { Store } from '@ngrx/store';
import { mergeMap, withLatestFrom } from 'rxjs/operators';
import { downloadPdf } from '@league-view/utils';
import { HttpResponse } from '@angular/common/http';
import { CallState, ErrorState } from '@libs/utils';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root',
})
export class PdfExportService {
  downloadRaceExport$: Subject<string> = new Subject<string>();
  downloadHandler$: Observable<boolean> = this.downloadRaceExport$.pipe(
    withLatestFrom(this.store.select(selectLeagueId)),
    mergeMap(this.fetchPdf.bind(this)),
    mergeMap(downloadPdf),
    mergeMap((mergeResult: Blob | CallState) => {
      return (mergeResult as ErrorState).errorMessage !== undefined
        ? of(this.handleError(mergeResult as ErrorState))
        : of(true);
    })
  );

  constructor(
    private readonly defaultService: DefaultService,
    private readonly store: Store,
    private readonly messageService: MessageService
  ) {}

  private fetchPdf([raceId, leagueId]: [string, string]): Observable<HttpResponse<Blob>> {
    return this.defaultService.leagueLeagueIdRaceresultRaceIdRaceExportGet(
      leagueId,
      raceId,
      'response'
    );
  }

  private handleError(result: ErrorState): boolean {
    this.messageService.add({
      severity: 'error',
      summary: 'An Error Occured!',
      detail: `${result.errorMessage}`,
      life: 5000,
    });
    return false;
  }
}
