import { LEAGUE_FEATURE_KEY, leagueReducer, LeagueState } from './league/league.reducer';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import { FILTER_FEATURE_KEY, filterReducer, FilterState } from './filters/filter.reducer';

export const LEAGUE_VIEW_FEATURE_KEY = 'league-view';

export interface LeagueViewState {
  [LEAGUE_FEATURE_KEY]: LeagueState;
  [FILTER_FEATURE_KEY]: FilterState;
}

export const leagueViewReducers: ActionReducerMap<LeagueViewState> = {
  [LEAGUE_FEATURE_KEY]: leagueReducer,
  [FILTER_FEATURE_KEY]: filterReducer,
};

export const selectLeagueViewState = createFeatureSelector<LeagueViewState>(
  LEAGUE_VIEW_FEATURE_KEY
);
