import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { StandingRow } from '@generated/openapi';
import { STANDINGS_ENTITY_KEY } from '@libs/utils';

@Injectable()
export class StandingsEntityService extends EntityCollectionServiceBase<StandingRow> {
  constructor(private serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(STANDINGS_ENTITY_KEY, serviceElementsFactory);
  }
}
