import { Injectable } from '@angular/core';
import { DefaultDataService, HttpUrlGenerator } from '@ngrx/data';
import { DefaultService, StandingRow } from '@generated/openapi';
import { HttpClient } from '@angular/common/http';
import { STANDINGS_ENTITY_KEY } from '@libs/utils';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { selectLeagueId } from '@libs/league-view-data-access';
import { filter, switchMap } from 'rxjs/operators';
import { notEmpty } from '@libs/utils';

@Injectable()
export class StandingsDataService extends DefaultDataService<StandingRow> {
  constructor(
    public http: HttpClient,
    public httpUrlGenerator: HttpUrlGenerator,
    private defaultService: DefaultService,
    private store: Store
  ) {
    super(STANDINGS_ENTITY_KEY, http, httpUrlGenerator);
  }

  getAll(): Observable<StandingRow[]> {
    return this.store.select(selectLeagueId).pipe(
      filter(notEmpty),
      switchMap((leagueId) => this.defaultService.leagueLeagueIdStandingGet(leagueId))
    );
  }
}
