import { createSelector } from '@ngrx/store';
import { extractErrorIfPresent, isLoaded, isLoading } from '@libs/utils';
import { selectLeagueViewState } from '../index';
import { LEAGUE_FEATURE_KEY } from './league.reducer';

export const selectLeagueState = createSelector(
  selectLeagueViewState,
  (state) => state[LEAGUE_FEATURE_KEY]
);

export const selectLoading = createSelector(selectLeagueState, (state) =>
  isLoading(state.callState)
);

export const selectLoaded = createSelector(selectLeagueState, (state) => isLoaded(state.callState));

export const selectError = createSelector(selectLeagueState, (state) =>
  extractErrorIfPresent(state.callState)
);

export const selectLeagueId = createSelector(selectLeagueState, (state) => state.leagueId);

export const selectLeagueName = createSelector(selectLeagueState, (state) => state.leagueName);

export const selectDriversByDriverId = createSelector(
  selectLeagueState,
  (state) => state.driversByDriverId
);

export const selectRacesByRaceId = createSelector(
  selectLeagueState,
  (state) => state.racesByRaceId
);
