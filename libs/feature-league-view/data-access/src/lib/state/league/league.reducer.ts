import { createReducer, on } from '@ngrx/store';
import { F1Driver, Race } from '@generated/openapi';
import { CallState, LoadingState } from '@libs/utils';
import { LeagueActions } from './league-view-types';

export const LEAGUE_FEATURE_KEY = 'league';

export interface LeagueState {
  leagueId: string;
  leagueName: string;
  driversByDriverId: Record<string, F1Driver>;
  racesByRaceId: Record<string, Race>;
  callState: CallState;
}

export const initialLeagueState: LeagueState = {
  leagueId: '',
  leagueName: '',
  driversByDriverId: {},
  racesByRaceId: {},
  callState: LoadingState.INIT,
};

export const leagueReducer = createReducer(
  initialLeagueState,
  on(LeagueActions.leagueSelected, (state, action) => ({
    ...state,
    callState: LoadingState.LOADING,
    leagueId: action.leagueId,
  })),
  on(LeagueActions.leagueResolved, (state, action) => ({
    ...state,
    callState: LoadingState.LOADED,
    leagueName: action.fullLeague.leagueName,
    driversByDriverId: action.fullLeague.driversByDriverId,
    racesByRaceId: action.fullLeague.racesByRaceId,
  })),
  on(LeagueActions.leagueError, (state, action) => ({
    ...state,
    callState: { errorMessage: action.errorMessage },
  })),
  on(LeagueActions.clearLeague, () => initialLeagueState)
);
