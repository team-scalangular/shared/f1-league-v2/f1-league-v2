import { Injectable } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { StandingsEntityService } from '../standings/standings-entity.service';
import { RaceResultEntityService } from '../race-results/race-result-entity.service';
import { debounceTime, filter, map, mergeMap, switchMap, take } from 'rxjs/operators';
import {
  activeFilterFrom,
  selectInformationForDriversSingleFrom,
  selectInformationForRacesAllFrom,
  selectInformationForSingleRaceFrom,
  selectInformationForStandingsFrom,
  selectLoadedFlags,
  selectLoadingFlags,
} from './league-facede.selectors';
import { selectDriversByDriverId, selectLeagueId, selectRacesByRaceId } from './league.selectors';
import { FilterActions } from '../filters/filter-types';
import {
  DRFilters_enum,
  extractNeededDriverInfosFromRecord,
  extractRacesFromRecord,
  FilterDisplayInfo,
  FilterSelectData,
  LeagueData,
  leagueDataBy,
  LeagueInfoDriversAll,
  LeagueInfoDriversSingle,
  LeagueInfoRacesAll,
  LeagueInfoRacesSingle,
  TableData,
  toFilterSelectInfo,
  toRacesAll,
  toSingleDriverResult,
  toSingleRace,
  toStandings,
  toTableData,
} from '@league-view/utils';
import { notEmpty, toLoadedFlag, toSingleLoadingFlag } from '@libs/utils';
import { Race, RaceResult } from '@generated/openapi';

@Injectable()
export class LeagueFacade {
  private standings$: Observable<LeagueInfoDriversAll[]> = combineLatest(
    selectInformationForStandingsFrom(this.store, this.standingsService)
  ).pipe(map(toStandings));

  private driversSingle$: Observable<LeagueInfoDriversSingle[]> = combineLatest(
    selectInformationForDriversSingleFrom(this.store, this.raceResultService)
  ).pipe(map(toSingleDriverResult));

  private racesAll$: Observable<LeagueInfoRacesAll[]> = combineLatest(
    selectInformationForRacesAllFrom(this.store, this.raceResultService)
  ).pipe(map(toRacesAll));

  private racesSingle$: Observable<LeagueInfoRacesSingle[]> = combineLatest(
    selectInformationForSingleRaceFrom(this.store, this.raceResultService)
  ).pipe(map(toSingleRace));

  private driversById$: Observable<FilterDisplayInfo[]> = this.store
    .select(selectDriversByDriverId)
    .pipe(filter(notEmpty), map(extractNeededDriverInfosFromRecord));

  private racesById$: Observable<FilterDisplayInfo[]> = this.store
    .select(selectRacesByRaceId)
    .pipe(filter(notEmpty), map(extractRacesFromRecord));

  private leagueId$: Observable<string> = this.store.select(selectLeagueId);

  public defaultRacesById$: Observable<Record<string, Race>> =
    this.store.select(selectRacesByRaceId);

  public defaultRaceResults$: Observable<RaceResult[]> = this.raceResultService.entities$;

  public loadingLeagueView$: Observable<boolean> = combineLatest(
    selectLoadingFlags(this.store, this.standingsService, this.raceResultService)
  ).pipe(map(toSingleLoadingFlag));

  public loadedLeagueView$: Observable<boolean> = combineLatest(
    selectLoadedFlags(this.store, this.standingsService, this.raceResultService)
  ).pipe(map(toLoadedFlag));

  public tableData$: Observable<TableData> = combineLatest(activeFilterFrom(this.store)).pipe(
    map(toTableData),
    filter(notEmpty)
  );

  public leagueData$: Observable<LeagueData> = this.loadedLeagueView$.pipe(
    filter((loaded: boolean) => loaded),
    switchMap(() => combineLatest(activeFilterFrom(this.store))),
    debounceTime(200),
    mergeMap((filter: [DRFilters_enum, string]) =>
      leagueDataBy(filter, this.standings$, this.driversSingle$, this.racesAll$, this.racesSingle$)
    )
  );

  public filterSelectData$: Observable<FilterSelectData> = combineLatest([
    ...activeFilterFrom(this.store),
    this.driversById$,
    this.racesById$,
  ]).pipe(map(toFilterSelectInfo));

  constructor(
    private store: Store,
    private standingsService: StandingsEntityService,
    private raceResultService: RaceResultEntityService
  ) {}

  public updateDriverFilter(filterEnum: DRFilters_enum): void {
    this.store.dispatch(FilterActions.setDRFilter({ activeDRFilter: filterEnum }));
  }

  public updateEntityFilter(entityFilter: string): void {
    this.store.dispatch(FilterActions.setEntityFilter({ activeEntityFilter: entityFilter }));
  }

  public getLeagueIdAsPromise(): Promise<string> {
    return this.leagueId$.pipe(take(1)).toPromise();
  }
}
