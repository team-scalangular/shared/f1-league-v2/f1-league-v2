import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  selectDriversByDriverId,
  selectLoaded,
  selectLoading,
  selectRacesByRaceId,
} from './league.selectors';
import { RaceResultEntityService } from '../race-results/race-result-entity.service';
import { F1Driver, Race, RaceResult, StandingRow } from '@generated/openapi';
import { StandingsEntityService } from '../standings/standings-entity.service';
import { selectDRFilter, selectEntityFilter } from '../filters/filter.selectors';
import { DRFilters_enum } from '@league-view/utils';

export const activeFilterFrom = (
  store: Store
): [Observable<DRFilters_enum>, Observable<string>] => [
  store.select(selectDRFilter),
  store.select(selectEntityFilter),
];

export const selectLoadingFlags = (
  store: Store,
  standingsService: StandingsEntityService,
  raceResultService: RaceResultEntityService
): [Observable<boolean>, Observable<boolean>, Observable<boolean>] => [
  store.select(selectLoading),
  standingsService.loading$,
  raceResultService.loading$,
];

export const selectLoadedFlags = (
  store: Store,
  standingsService: StandingsEntityService,
  raceResultService: RaceResultEntityService
): [Observable<boolean>, Observable<boolean>, Observable<boolean>] => [
  store.select(selectLoaded),
  standingsService.loaded$,
  raceResultService.loaded$,
];

export const selectInformationForStandingsFrom = (
  store: Store,
  standingsService: StandingsEntityService
): [Observable<Record<string, F1Driver>>, Observable<StandingRow[]>] => [
  store.select(selectDriversByDriverId),
  standingsService.entities$,
];

export const selectInformationForDriversSingleFrom = (
  store: Store,
  raceResultService: RaceResultEntityService
): [
  Observable<string>,
  Observable<RaceResult[]>,
  Observable<Record<string, F1Driver>>,
  Observable<Record<string, Race>>
] => [
  store.select(selectEntityFilter),
  raceResultService.entities$,
  store.select(selectDriversByDriverId),
  store.select(selectRacesByRaceId),
];

export const selectInformationForRacesAllFrom = (
  store: Store,
  raceResultsService: RaceResultEntityService
): [
  Observable<Record<string, F1Driver>>,
  Observable<Record<string, Race>>,
  Observable<RaceResult[]>
] => [
  store.select(selectDriversByDriverId),
  store.select(selectRacesByRaceId),
  raceResultsService.entities$,
];

export const selectInformationForSingleRaceFrom = (
  store: Store,
  raceResultService: RaceResultEntityService
): [
  Observable<string>,
  Observable<Record<string, F1Driver>>,
  Observable<Record<string, Race>>,
  Observable<RaceResult[]>
] => [
  store.select(selectEntityFilter),
  store.select(selectDriversByDriverId),
  store.select(selectRacesByRaceId),
  raceResultService.entities$,
];
