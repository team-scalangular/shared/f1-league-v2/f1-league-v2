import { initialLeagueState } from '../league.reducer';
import { LeagueEffects } from '../league.effects';
import { Observable, throwError } from 'rxjs';
import { provideMockStore } from '@ngrx/store/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { DefaultService } from '@generated/openapi';
import { StandingsEntityService } from '../../standings/standings-entity.service';
import { RaceResultEntityService } from '../../race-results/race-result-entity.service';
import { LeagueActions } from '../league-view-types';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries,@typescript-eslint/no-unused-vars
import * as DataCreators from '../../../../../../utils/src/lib/functions/data-creators';
import {
  createFullLeague,
  mockDriverByDriverId,
  mockRacesByRaceId,
  shortLeagueMock,
  shortRacesMock,
  standingsMock,
} from '@league-view/utils';
import { cold, hot } from 'jest-marbles';
import { createSpyObj } from '../../../../../../../../helper';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';

describe('LeagueEffects', () => {
  const initialState = initialLeagueState;
  const defaultService = createSpyObj('defaultService', [
    'leagueLeagueIdGet',
    'leagueLeagueIdRacesGet',
  ]);
  const standingsEntityService = createSpyObj('standingsEntityService', ['getAll']);
  const raceResultsEntityService = createSpyObj('raceResultsEntityService', ['getAll']);
  const dataCreatorsMock = jest
    .spyOn(DataCreators, 'createFullLeague')
    .mockImplementation(() => fullLeague);
  const fullLeague = {
    leagueName: shortLeagueMock.name,
    driversByDriverId: mockDriverByDriverId,
    racesByRaceId: mockRacesByRaceId,
  };
  let effects: LeagueEffects;
  let metadata: EffectsMetadata<LeagueEffects>;
  let actions: Observable<unknown>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LeagueEffects,
        provideMockActions(() => actions),
        provideMockStore({ initialState }),
        { provide: DefaultService, useValue: defaultService },
        { provide: StandingsEntityService, useValue: standingsEntityService },
        {
          provide: RaceResultEntityService,
          useValue: raceResultsEntityService,
        },
      ],
    });

    effects = TestBed.inject(LeagueEffects);
    metadata = getEffectsMetadata(effects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('leagueSelected$', () => {
    it('should return fullLeague on leagueSelected$', () => {
      const action = LeagueActions.leagueSelected({
        leagueId: '4ccc5a4f-bdcb-4bd5-a7e8-2382adf804e2',
      });
      const outcome = LeagueActions.leagueResolved({ fullLeague });
      actions = hot('-a', { a: action });
      const responseLeague = cold('-b|', { b: shortLeagueMock });
      const responseRaces = cold('-c|', { c: shortRacesMock });

      defaultService.leagueLeagueIdGet.mockReturnValue(responseLeague);
      defaultService.leagueLeagueIdRacesGet.mockReturnValue(responseRaces);

      expect(effects.leagueSelected$).toBeObservable(hot('---b', { b: outcome }));
      expect(effects.leagueSelected$).toSatisfyOnFlush(() => {
        expect(dataCreatorsMock.mockImplementation(createFullLeague)).toHaveBeenCalledWith(
          shortLeagueMock,
          shortRacesMock
        );
      });
    });

    it('should return the error action on error leagueGet', () => {
      const errorMessage = 'Error';
      const action = LeagueActions.leagueSelected({
        leagueId: '4ccc5a4f-bdcb-4bd5-a7e8-2382adf804e2',
      });
      const outcome = LeagueActions.leagueError({ errorMessage });

      actions = hot('-a', { a: action });
      const responseRaces = cold('-c|', { c: shortRacesMock });

      defaultService.leagueLeagueIdGet.mockReturnValue(throwError({ message: errorMessage }));
      defaultService.leagueLeagueIdRacesGet.mockReturnValue(responseRaces);

      expect(effects.leagueSelected$).toBeObservable(cold('-a', { a: outcome }));
    });

    it('should return the error action on error racesGet', () => {
      const errorMessage = 'Error';
      const action = LeagueActions.leagueSelected({
        leagueId: '4ccc5a4f-bdcb-4bd5-a7e8-2382adf804e2',
      });
      const outcome = LeagueActions.leagueError({ errorMessage });

      actions = hot('-a', { a: action });
      const responseLeague = cold('-b|', { b: shortLeagueMock });

      defaultService.leagueLeagueIdGet.mockReturnValue(responseLeague);
      defaultService.leagueLeagueIdRacesGet.mockReturnValue(throwError({ message: errorMessage }));

      expect(effects.leagueSelected$).toBeObservable(cold('-a', { a: outcome }));
    });
  });

  describe('getStandingsAndRaceResults$', () => {
    it('should not dispatch', () => {
      expect(metadata.getStandingsAndRaceResults$).toEqual(
        expect.objectContaining({ dispatch: false })
      );
    });

    it('should call the entity services getAll methods', () => {
      const action = LeagueActions.leagueResolved({ fullLeague });

      standingsEntityService.getAll.mockReturnValue(standingsMock);

      actions = hot('a', { a: action });

      expect(effects.getStandingsAndRaceResults$).toBeObservable(cold('a', { a: action }));
      expect(effects.getStandingsAndRaceResults$).toSatisfyOnFlush(() => {
        expect(standingsEntityService.getAll).toHaveBeenCalled();
        expect(raceResultsEntityService.getAll).toHaveBeenCalled();
      });
    });
  });
});
