import {
  initialLeagueState,
  LeagueState,
  selectDriversByDriverId,
  selectError,
  selectLeagueId,
  selectLeagueName,
  selectLoaded,
  selectLoading,
  selectRacesByRaceId,
} from '@libs/league-view-data-access';
import { mockDriverByDriverId, mockRacesByRaceId } from '@league-view/utils';
import { LoadingState } from '@libs/utils';

describe('LeagueSelectors', () => {
  describe('selectCallState', () => {
    let initialState: LeagueState;
    beforeEach(() => {
      initialState = initialLeagueState;
    });

    it('it should return false, false, null for init', () => {
      const loading = selectLoading.projector(initialState);
      const loaded = selectLoaded.projector(initialState);
      const error = selectError.projector(initialState);

      expect(loading).toEqual(false);
      expect(loaded).toEqual(false);
      expect(error).toBe(null);
    });

    it('should return true, false, null for loading', () => {
      initialState = {
        ...initialState,
        callState: LoadingState.LOADING,
      };

      const loading = selectLoading.projector(initialState);
      const loaded = selectLoaded.projector(initialState);
      const error = selectError.projector(initialState);

      expect(loading).toEqual(true);
      expect(loaded).toEqual(false);
      expect(error).toBe(null);
    });

    it('should return false, true, null for loaded', () => {
      initialState = {
        ...initialState,
        callState: LoadingState.LOADED,
      };

      const loading = selectLoading.projector(initialState);
      const loaded = selectLoaded.projector(initialState);
      const error = selectError.projector(initialState);

      expect(loading).toEqual(false);
      expect(loaded).toEqual(true);
      expect(error).toBe(null);
    });

    it('should return false, false, errorMessage for loaded', () => {
      initialState = {
        ...initialState,
        callState: { errorMessage: 'Error' },
      };

      const loading = selectLoading.projector(initialState);
      const loaded = selectLoaded.projector(initialState);
      const error = selectError.projector(initialState);

      expect(loading).toEqual(false);
      expect(loaded).toEqual(false);
      expect(error).toEqual('Error');
    });
  });

  describe('selectLeagueId', () => {
    it('should return the leagueId', () => {
      const initialState = {
        ...initialLeagueState,
        leagueId: '4ccc5a4f-bdcb-4bd5-a7e8-2382adf804e2',
      };

      const leagueId = selectLeagueId.projector(initialState);

      expect(leagueId).toEqual(initialState.leagueId);
    });
  });

  describe('selectLeagueName', () => {
    it('should return the leagueName', () => {
      const initialState = {
        ...initialLeagueState,
        leagueName: 'Test',
      };

      const leagueName = selectLeagueName.projector(initialState);

      expect(leagueName).toEqual(initialState.leagueName);
    });
  });

  describe('selectRecords', () => {
    it('should return the drivers record', () => {
      const initialState = {
        ...initialLeagueState,
        driversByDriverId: mockDriverByDriverId,
      };

      const driversById = selectDriversByDriverId.projector(initialState);

      expect(driversById).toEqual(initialState.driversByDriverId);
      expect(Object.keys(driversById)).toHaveLength(2);
    });

    it('should return the races record', () => {
      const initialState = {
        ...initialLeagueState,
        racesByRaceId: mockRacesByRaceId,
      };

      const racesById = selectRacesByRaceId.projector(initialState);

      expect(racesById).toEqual(initialState.racesByRaceId);
      expect(Object.keys(racesById)).toHaveLength(2);
    });
  });
});
