import { initialLeagueState, leagueReducer, LeagueState } from '@libs/league-view-data-access';
import { LeagueActions } from '../league-view-types';
import { extractErrorIfPresent, LoadingState } from '@libs/utils';
import { leagueViewMocks, mockDriverByDriverId, mockRacesByRaceId } from '@league-view/utils';

describe('LeagueReducer', () => {
  describe('Unknown Action', () => {
    it('should return the initialState for unknown actions', () => {
      const initialState = initialLeagueState;
      const action = { type: 'Unknown' };

      const state = leagueReducer(initialState, action);

      expect(state).toEqual(initialState);
    });
  });

  describe('leagueSelected', () => {
    let initialState: LeagueState;
    let action;
    let state: LeagueState;

    beforeEach(() => {
      initialState = initialLeagueState;
      action = LeagueActions.leagueSelected({
        leagueId: leagueViewMocks.leagueId,
      });
      state = leagueReducer(initialState, action);
    });

    it('should set the corresponding leagueId', () => {
      expect(state.leagueId).toEqual(leagueViewMocks.leagueId);
    });

    it('should set the callState to loading', () => {
      expect(state.callState).toEqual(LoadingState.LOADING);
    });
  });

  describe('LeagueResolved', () => {
    let initialState: LeagueState;
    let action;
    let state: LeagueState;

    beforeEach(() => {
      initialState = initialLeagueState;
      action = LeagueActions.leagueResolved({
        fullLeague: {
          leagueName: leagueViewMocks.name,
          driversByDriverId: mockDriverByDriverId,
          racesByRaceId: mockRacesByRaceId,
        },
      });
      state = leagueReducer(initialState, action);
    });

    it('should set the correct leagueName', () => {
      expect(state.leagueName).toEqual(leagueViewMocks.name);
    });
    it('should set the correct CallState', () => {
      expect(state.callState).toEqual(LoadingState.LOADED);
    });
    it('should set the records', () => {
      expect(state.driversByDriverId).toEqual(mockDriverByDriverId);
      expect(state.racesByRaceId).toEqual(mockRacesByRaceId);
    });
  });

  describe('LeagueError', () => {
    it('should set the errorMessage as CallState', () => {
      const initialState = initialLeagueState;
      const action = LeagueActions.leagueError({ errorMessage: 'Error' });

      const state = leagueReducer(initialState, action);

      expect(extractErrorIfPresent(state.callState)).toEqual('Error');
    });
  });
});
