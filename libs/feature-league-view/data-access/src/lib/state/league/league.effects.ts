import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { DefaultService, League, Race } from '@generated/openapi';
import { LeagueActions } from './league-view-types';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { StandingsEntityService } from '../standings/standings-entity.service';
import { RaceResultEntityService } from '../race-results/race-result-entity.service';
import { createFullLeague } from '@league-view/utils';

@Injectable()
export class LeagueEffects {
  leagueSelected$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LeagueActions.leagueSelected),
      map((action) => action.leagueId),
      switchMap((leagueId) =>
        forkJoin([
          this.defaultService.leagueLeagueIdGet(leagueId),
          this.defaultService.leagueLeagueIdRacesGet(leagueId),
        ]).pipe(
          map(([league, races]: [League, Race[]]) => createFullLeague(league, races)),
          map((fullLeague) => LeagueActions.leagueResolved({ fullLeague })),
          catchError((error) => of(LeagueActions.leagueError({ errorMessage: error.message })))
        )
      )
    )
  );

  getStandingsAndRaceResults$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(LeagueActions.leagueResolved),
        tap(() => {
          this.standingsEntityService.getAll();
          this.raceResultsEntityService.getAll();
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private defaultService: DefaultService,
    private standingsEntityService: StandingsEntityService,
    private raceResultsEntityService: RaceResultEntityService
  ) {}
}
