import { createAction, props } from '@ngrx/store';
import { FullLeague } from '@league-view/utils';

export const leagueSelected = createAction(
  '[League View] League Selected',
  props<{ leagueId: string }>()
);

export const leagueResolved = createAction(
  '[League API] League and Races Resolved Success',
  props<{ fullLeague: FullLeague }>()
);

export const leagueError = createAction(
  '[League API] League or Races Resolve Error',
  props<{ errorMessage: string }>()
);

export const clearLeague = createAction('[League View] Clear existing league');
