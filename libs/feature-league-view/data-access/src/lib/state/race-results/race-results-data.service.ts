import { Injectable } from '@angular/core';
import { DefaultDataService, HttpUrlGenerator } from '@ngrx/data';
import { DefaultService, RaceResult } from '@generated/openapi';
import { HttpClient } from '@angular/common/http';
import { notEmpty, RACE_RESULTS_ENTITY_KEY } from '@libs/utils';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { selectLeagueId } from '@libs/league-view-data-access';
import { filter, switchMap } from 'rxjs/operators';

@Injectable()
export class RaceResultsDataService extends DefaultDataService<RaceResult> {
  constructor(
    public http: HttpClient,
    public httpUrlGenerator: HttpUrlGenerator,
    private defaultService: DefaultService,
    private store: Store
  ) {
    super(RACE_RESULTS_ENTITY_KEY, http, httpUrlGenerator);
  }

  getAll(): Observable<RaceResult[]> {
    return this.store.select(selectLeagueId).pipe(
      filter(notEmpty),
      switchMap((leagueId) => this.defaultService.leagueLeagueIdRaceresultsGet(leagueId))
    );
  }
}
