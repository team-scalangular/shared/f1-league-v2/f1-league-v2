import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { RaceResult } from '@generated/openapi';
import { RACE_RESULTS_ENTITY_KEY } from '@libs/utils';

@Injectable()
export class RaceResultEntityService extends EntityCollectionServiceBase<RaceResult> {
  constructor(private serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super(RACE_RESULTS_ENTITY_KEY, serviceElementsFactory);
  }
}
