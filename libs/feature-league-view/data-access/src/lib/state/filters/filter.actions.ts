import { createAction, props } from '@ngrx/store';
import { DRFilters_enum } from '@league-view/utils';

export const setDRFilter = createAction(
  '[DR Filter Box] DR Filter Updated',
  props<{ activeDRFilter: DRFilters_enum }>()
);

export const setEntityFilter = createAction(
  '[Entity Filter Box] Entity Filter Updated',
  props<{ activeEntityFilter: string }>()
);

export const resetFilter = createAction('[League View] Reset Filter');
