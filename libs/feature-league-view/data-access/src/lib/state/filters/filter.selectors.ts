import { createSelector } from '@ngrx/store';
import { selectLeagueViewState } from '../index';
import { FILTER_FEATURE_KEY } from './filter.reducer';

export const selectFilterState = createSelector(
  selectLeagueViewState,
  (state) => state[FILTER_FEATURE_KEY]
);

export const selectDRFilter = createSelector(selectFilterState, (state) => state.activeDRFilter);

export const selectEntityFilter = createSelector(
  selectFilterState,
  (state) => state.activeEntityFilter
);
