import { createReducer, on } from '@ngrx/store';
import { FilterActions } from './filter-types';
import { DRFilters_enum } from '@league-view/utils';

export const FILTER_FEATURE_KEY = 'filter';

export interface FilterState {
  activeDRFilter: DRFilters_enum;
  activeEntityFilter: string;
}

export const initialFilterState: FilterState = {
  activeDRFilter: DRFilters_enum.FILTER_DRIVERS,
  activeEntityFilter: '',
};

export const filterReducer = createReducer(
  initialFilterState,
  on(FilterActions.setDRFilter, (state, action) => ({
    ...state,
    activeDRFilter: action.activeDRFilter,
    activeEntityFilter: initialFilterState.activeEntityFilter,
  })),
  on(FilterActions.setEntityFilter, (state, action) => ({
    ...state,
    activeEntityFilter: action.activeEntityFilter,
  })),
  on(FilterActions.resetFilter, () => initialFilterState)
);
