import { filterReducer, FilterState, initialFilterState } from '@libs/league-view-data-access';
import { FilterActions } from '../filter-types';
import { DRFilters_enum } from '@league-view/utils';

describe('FilterReducer', () => {
  let initialState: FilterState;
  let action;
  let state: FilterState | Record<string, never>;

  beforeEach(() => {
    initialState = initialFilterState;
    action = {};
    state = {};
  });

  it('should return initialState for Unknown action', () => {
    action = { type: 'Unknown' };

    state = filterReducer(initialState, action);

    expect(state).toEqual(initialState);
  });

  it('should set DRFilter for setDRFilter', () => {
    action = FilterActions.setDRFilter({
      activeDRFilter: DRFilters_enum.FILTER_RACES,
    });

    state = filterReducer(initialState, action);

    expect(state.activeDRFilter).toEqual(DRFilters_enum.FILTER_RACES);
    expect(state.activeEntityFilter).toEqual('');
  });

  it('should set the entityId for setEntityFilter', () => {
    action = FilterActions.setEntityFilter({
      activeEntityFilter: '7b276df0-ff99-4082-acda-830136a20e7a',
    });

    state = filterReducer(initialState, action);

    expect(state.activeEntityFilter).toEqual('7b276df0-ff99-4082-acda-830136a20e7a');
    expect(state.activeDRFilter).toEqual(DRFilters_enum.FILTER_DRIVERS);
  });
});
