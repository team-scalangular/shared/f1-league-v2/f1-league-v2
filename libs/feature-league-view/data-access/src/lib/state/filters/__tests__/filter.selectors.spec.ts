import {
  initialFilterState,
  selectDRFilter,
  selectEntityFilter,
} from '@libs/league-view-data-access';

describe('FilterSelector', () => {
  it('should return the current filter', () => {
    const initialState = initialFilterState;

    const drFilter = selectDRFilter.projector(initialState);
    const entityFilter = selectEntityFilter.projector(initialState);

    expect(drFilter).toEqual(initialState.activeDRFilter);
    expect(entityFilter).toEqual(initialState.activeEntityFilter);
  });
});
