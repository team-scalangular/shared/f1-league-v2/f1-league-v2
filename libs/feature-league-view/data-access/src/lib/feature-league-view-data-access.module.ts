import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LeagueEffects } from './state/league/league.effects';
import { LeagueFacade } from './state/league/league.facade';
import { StandingsEntityService } from './state/standings/standings-entity.service';
import { StandingsDataService } from './state/standings/standings-data.service';
import { EntityDataService } from '@ngrx/data';
import { RaceResultsDataService } from './state/race-results/race-results-data.service';
import { RaceResultEntityService } from './state/race-results/race-result-entity.service';
import { LEAGUE_VIEW_FEATURE_KEY, leagueViewReducers } from './state';
import { RACE_RESULTS_ENTITY_KEY, STANDINGS_ENTITY_KEY } from '@libs/utils';
import { RouteMetadataProvider } from './services/route-metadata-provider.service';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(LEAGUE_VIEW_FEATURE_KEY, leagueViewReducers),
    EffectsModule.forFeature([LeagueEffects]),
  ],
  providers: [
    LeagueFacade,
    StandingsEntityService,
    StandingsDataService,
    RaceResultEntityService,
    RaceResultsDataService,
    RouteMetadataProvider,
  ],
})
export class FeatureLeagueViewDataAccessModule {
  constructor(
    private entityDataService: EntityDataService,
    private standingsDataService: StandingsDataService,
    private raceResultsDataService: RaceResultsDataService
  ) {
    entityDataService.registerServices({
      [STANDINGS_ENTITY_KEY]: standingsDataService,
      [RACE_RESULTS_ENTITY_KEY]: raceResultsDataService,
    });
  }
}
