import { F1Driver, Race } from '@generated/openapi';

export interface FullLeague {
  leagueName: string;
  driversByDriverId: Record<string, F1Driver>;
  racesByRaceId: Record<string, Race>;
}
