import { F1Circuit, F1RaceResult } from '@generated/openapi';

export enum TableData_enum {
  DRIVERS_ALL = 'Drivers All',
  DRIVERS_SINGLE = 'Drivers Single',
  RACES_ALL = 'Races All',
  RACES_SINGLE = 'Races Single',
}

export interface TableData_Drivers_All {
  field: TableData_Drivers_All_Fields;
  header: TableData_Drivers_All_Header;
}

export type TableData_Drivers_All_Fields = 'position' | 'fullName' | 'carNumber' | 'points';

export type TableData_Drivers_All_Header = 'Position' | 'Name' | 'Car Number' | 'Points';

export interface LeagueInfoDriversAll {
  position: number;
  fullName: string;
  carNumber: number;
  points: number;
}

export interface TableData_Drivers_Single {
  field: TableData_Drivers_Single_Fields;
  header: TableData_Drivers_Single_Header;
}

export type TableData_Drivers_Single_Fields =
  | 'grandPrix'
  | 'date'
  | 'endPosition'
  | 'gridPosition'
  | 'pitStops'
  | 'fastestLap'
  | 'totalTime'
  | 'penaltyAmount'
  | 'penaltyTime'
  | 'points';

export type TableData_Drivers_Single_Header =
  | 'Grand Prix'
  | 'Date'
  | 'End Position'
  | 'Grid Position'
  | 'Pit Stops'
  | 'Fastest Lap'
  | 'Total Time'
  | 'Penalties Amount'
  | 'Penalty Time'
  | 'Points';

export interface LeagueInfoDriversSingle {
  grandPrix: F1Circuit;
  date: string;
  endPosition: number | string;
  gridPosition: number | string;
  pitStops: number | string;
  fastestLap: number | string;
  totalTime: number | string;
  penaltyAmount: number | string;
  penaltyTime: number | string;
  points: number | string;
  raceResult: F1RaceResult;
  isFastestLap: boolean;
}

export interface TableData_Races_All {
  field: TableData_Races_All_Fields;
  header: TableData_Races_All_Header;
}

export type TableData_Races_All_Fields = 'grandPrix' | 'date' | 'winner' | 'laps' | 'raceTime';

export type TableData_Races_All_Header = 'Grand Prix' | 'Date' | 'Winner' | 'Laps' | 'Race Time';

export interface LeagueInfoRacesAll {
  grandPrix: F1Circuit;
  date: string;
  winner: string;
  laps: number | string;
  raceTime: number | string;
}

export interface TableData_Races_Single {
  field: TableData_Races_Single_Fields;
  header: TableData_Races_Single_Header;
}

export type TableData_Races_Single_Fields =
  | 'position'
  | 'number'
  | 'driver'
  | 'gridPosition'
  | 'pitStops'
  | 'fastestLap'
  | 'totalTime'
  | 'penaltyAmount'
  | 'penaltyTime'
  | 'laps'
  | 'points';

export type TableData_Races_Single_Header =
  | 'Position'
  | 'Number'
  | 'Driver'
  | 'Grid Position'
  | 'Pit Stops'
  | 'Fastest Lap'
  | 'Total Time'
  | 'Penalties Amount'
  | 'Penalty Time'
  | 'Laps'
  | 'Points';

export interface LeagueInfoRacesSingle {
  position: number | string;
  number: number;
  driver: string;
  gridPosition: number | string;
  pitStops: number | string;
  fastestLap: number | string;
  totalTime: number | string;
  penaltyAmount: number | string;
  penaltyTime: number | string;
  laps: number | string;
  time: number | string;
  points: number;
  raceResult: F1RaceResult;
  isFastestLap: boolean;
}

export type TableData =
  | TableData_Drivers_All[]
  | TableData_Drivers_Single[]
  | TableData_Races_All[]
  | TableData_Races_Single[];

export type LeagueData =
  | LeagueInfoDriversAll[]
  | LeagueInfoDriversSingle[]
  | LeagueInfoRacesAll[]
  | LeagueInfoRacesSingle[];
