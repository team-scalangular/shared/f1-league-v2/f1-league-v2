export enum DRFilters_enum {
  FILTER_DRIVERS = 'Filter for drivers',
  FILTER_RACES = 'Filter for races',
}

export interface FilterSelectOptions {
  fName: string;
  fCode: string;
}

export interface FilterSelectData {
  activeDrFilter: DRFilters_enum;
  activeEntityFilter: string;
  driversToDisplay: FilterDisplayInfo[];
  racesToDisplay: FilterDisplayInfo[];
}

export type FilterDisplayInfo = { id: string; name: string; carNumber?: number };

export interface FilterConfig {
  name: string;
  code: string;
  childs: FilterSelectOptions[];
}
