import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { CallState } from '@libs/utils';

export const downloadPdf = (response: HttpResponse<Blob>): Observable<Blob | CallState> => {
  const fileName = fileNameFrom(response.headers);
  const blob = response.body;
  if (blob) {
    const url = window.URL.createObjectURL(response.body);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
    return of(blob);
  }
  return of({ errorMessage: 'Unable to resolve the pdf!' });
};

const fileNameFrom = (headers: HttpHeaders) => {
  const contentDisposition = headers.get('content-disposition');
  return contentDisposition ? contentDisposition.split('="')[1].slice(0, -1) : 'Race-Export';
};
