import {
  byEndPosition,
  completeDriverRaceResult,
  completeRaceResult,
  DRFilters_enum,
  FilterDisplayInfo,
  FilterSelectData,
  getCompleteRace,
  getFastestLap,
  getUnfinishedRaceResult,
  LeagueInfoDriversAll,
  LeagueInfoDriversSingle,
  LeagueInfoRacesAll,
  LeagueInfoRacesSingle,
  removeIrregularPosition,
  TableData,
  TableData_enum,
  TableDataTypesByTableDataEnum,
  unfinishedDriverRaceResult,
  unfinishedRaceResult,
} from '@league-view/utils';
import { DriverRaceResult, F1Driver, Race, RaceResult, StandingRow } from '@generated/openapi';

export const toTableData = ([drFilter, entityFilter]: [
  DRFilters_enum,
  string
]): TableData | null => {
  switch (drFilter) {
    case DRFilters_enum.FILTER_DRIVERS:
      if (entityFilter === '') {
        return TableDataTypesByTableDataEnum[TableData_enum.DRIVERS_ALL];
      } else {
        return TableDataTypesByTableDataEnum[TableData_enum.DRIVERS_SINGLE];
      }
    case DRFilters_enum.FILTER_RACES:
      if (entityFilter === '') {
        return TableDataTypesByTableDataEnum[TableData_enum.RACES_ALL];
      } else {
        return TableDataTypesByTableDataEnum[TableData_enum.RACES_SINGLE];
      }
    default:
      return null;
  }
};

export const toStandings = ([driversById, standings]: [
  Record<string, F1Driver>,
  StandingRow[]
]): LeagueInfoDriversAll[] =>
  standings.reduce(
    (fullStanding: LeagueInfoDriversAll[], standingRow: StandingRow): LeagueInfoDriversAll[] => [
      ...fullStanding,
      {
        fullName: `${driversById[standingRow.driverId].name} ${
          driversById[standingRow.driverId].surename
        }`,
        carNumber: driversById[standingRow.driverId].carNumber,
        position: standingRow.position,
        points: standingRow.points,
      },
    ],
    []
  );

export const toSingleDriverResult = ([entityFilter, raceResults, , racesById]: [
  string,
  RaceResult[],
  Record<string, F1Driver>,
  Record<string, Race>
]): LeagueInfoDriversSingle[] =>
  raceResults.flatMap<LeagueInfoDriversSingle>((result: RaceResult) => {
    const driverResult = result.driverResults.find(
      (dr: DriverRaceResult) => dr.driverId === entityFilter
    );
    if (driverResult === undefined) {
      return [];
    }
    const race = racesById[result.raceId];
    if (driverResult.raceResult !== 'finished' && driverResult.raceResult) {
      return unfinishedDriverRaceResult(race, driverResult);
    }
    const isFastestLap = getFastestLap(result) === driverResult.bestLapTime;
    return completeDriverRaceResult(race, driverResult, isFastestLap);
  });

export const toRacesAll = ([driverById, raceById, raceResults]: [
  Record<string, F1Driver>,
  Record<string, Race>,
  RaceResult[]
]): LeagueInfoRacesAll[] =>
  raceResults.flatMap<LeagueInfoRacesAll>((result: RaceResult) => {
    const race = raceById[result.raceId];
    const winner = result.driverResults.find((driver) => driver.endPosition === 1);
    if (race) {
      return winner ? getCompleteRace(race, driverById, winner) : getUnfinishedRaceResult(race);
    }
    return [];
  });

export const toSingleRace = ([entityFilter, driversById, , races]: [
  string,
  Record<string, F1Driver>,
  Record<string, Race>,
  RaceResult[]
]): LeagueInfoRacesSingle[] => {
  const race = races.find((race) => race.raceId === entityFilter);
  if (race) {
    return race.driverResults
      .slice()
      .map(removeIrregularPosition)
      .sort(byEndPosition)
      .flatMap((result) => {
        if (result.raceResult && result.raceResult !== 'finished') {
          return unfinishedRaceResult(result, driversById);
        }
        const isFastestLap = getFastestLap(race) === result.bestLapTime;
        return completeRaceResult(result, driversById, isFastestLap);
      });
  } else {
    return [];
  }
};

export const toFilterSelectInfo = ([
  activeDrFilter,
  activeEntityFilter,
  driversToDisplay,
  racesToDisplay,
]: [DRFilters_enum, string, FilterDisplayInfo[], FilterDisplayInfo[]]): FilterSelectData => ({
  activeDrFilter,
  activeEntityFilter,
  driversToDisplay,
  racesToDisplay,
});
