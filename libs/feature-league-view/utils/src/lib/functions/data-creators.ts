import { F1Driver, League, Race } from '@generated/openapi';
import {
  DRFilters_enum,
  FilterConfig,
  FilterDisplayInfo,
  FilterSelectOptions,
} from '../interfaces/filter';
import { FullLeague } from '../interfaces/league';
import { combineLatest, Observable } from 'rxjs';
import { LeagueViewTableState } from '../../../../ui/src/lib/league-view-table/league-view-table';
import { map } from 'rxjs/operators';
import { LeagueData, TableData } from '../interfaces/table';

export const createFullLeague = (league: League, races: Race[]): FullLeague => ({
  leagueName: league.name,
  driversByDriverId: createDriversRecord(league.drivers as F1Driver[]),
  racesByRaceId: createRaceRecord(races),
});

export const createDriversRecord = (drivers: F1Driver[]): Record<string, F1Driver> =>
  drivers.reduce(
    (driversById: Record<string, F1Driver>, driver: F1Driver) => ({
      ...driversById,
      [driver.driverId]: driver,
    }),
    {}
  );

export const createRaceRecord = (races: Race[]) =>
  races.reduce(
    (racesById: Record<string, Race>, race: Race) => ({
      ...racesById,
      [race.raceId]: race,
    }),
    {}
  );

export const createFilterConfig = (
  drivers: FilterDisplayInfo[],
  races: FilterDisplayInfo[]
): FilterConfig[] => [
  {
    name: 'Drivers',
    code: DRFilters_enum.FILTER_DRIVERS,
    childs: [
      {
        fName: 'Drivers - All',
        fCode: '',
      },
      ...drivers.map((driver: FilterDisplayInfo) => ({
        fName: `Driver - ${driver.name}`,
        fCode: driver.id,
      })),
    ],
  },
  {
    name: 'Races',
    code: DRFilters_enum.FILTER_RACES,
    childs: [
      {
        fName: 'Races - All',
        fCode: '',
      },
      ...races.map((race: FilterDisplayInfo) => ({
        fName: `Race - ${race.name}`,
        fCode: race.id,
      })),
    ],
  },
];

export const createSelectedFilter = (
  drFilter: DRFilters_enum,
  etFilter: string,
  driversToDisplay: FilterDisplayInfo[],
  racesToDisplay: FilterDisplayInfo[]
): FilterSelectOptions => {
  switch (drFilter) {
    case DRFilters_enum.FILTER_DRIVERS:
      if (etFilter === '') {
        return {
          fName: 'Drivers - All',
          fCode: '',
        };
      } else {
        const driver = driversToDisplay.find((dr) => dr.id === etFilter);
        return {
          fName: `Driver - ${driver?.name || 'UNKNOWN'}`,
          fCode: driver?.id || '',
        };
      }
    case DRFilters_enum.FILTER_RACES:
      if (etFilter === '') {
        return {
          fName: 'Races - All',
          fCode: '',
        };
      } else {
        const race = racesToDisplay.find((r) => r.id === etFilter);
        return {
          fName: `Race - ${race?.name || 'UNKNOWN'}`,
          fCode: race?.id || '',
        };
      }
    default:
      return {
        fName: 'Drivers - All',
        fCode: '',
      };
  }
};

export const createComponentState = (
  tableData$: Observable<TableData>,
  leagueData$: Observable<LeagueData>,
  loading$: Observable<boolean>
): Observable<LeagueViewTableState> =>
  combineLatest([tableData$, leagueData$, loading$]).pipe(
    map(([tableData, leagueData, loading]: [TableData, LeagueData, boolean]) => ({
      tableData,
      leagueData,
      loading,
    }))
  );
