import { NEVER, Observable } from 'rxjs';
import { DriverRaceResult, F1Driver, Race, RaceResult } from '@generated/openapi';
import { pointsByEndPosition } from '../static/maps';
import {
  LeagueData,
  LeagueInfoDriversAll,
  LeagueInfoDriversSingle,
  LeagueInfoRacesAll,
  LeagueInfoRacesSingle,
} from '../interfaces/table';
import { DRFilters_enum, FilterDisplayInfo } from '../interfaces/filter';

export const getFastestLap = (result: RaceResult): number =>
  result.driverResults
    .filter((drResult: DriverRaceResult) => drResult.bestLapTime !== -1)
    .reduce(
      (currentFastest: number, comparator: DriverRaceResult) =>
        comparator.bestLapTime < currentFastest ? comparator.bestLapTime : currentFastest,
      Number.MAX_SAFE_INTEGER
    );

/*
TODO check if this approach can be somehow implemented regularly without the bug in firefox
const byEndPosition = (a: DriverRaceResult, b: DriverRaceResult): number =>
  a.endPosition === -1 || b.endPosition === -1 ? 1 : a.endPosition - b.endPosition;
*/
export const byEndPosition = (a: DriverRaceResult, b: DriverRaceResult): number =>
  a.endPosition - b.endPosition;

export const calculatePointsBy = (endPosition: number, hasFastestLap: boolean): number =>
  endPosition > 10
    ? 0
    : hasFastestLap
    ? pointsByEndPosition[endPosition] + 1
    : pointsByEndPosition[endPosition];

// make position -1 so high, that the sorting will put it to the end
// this is a workaround because of the weird handling from sort in firefox and chrome
export const removeIrregularPosition = (result: DriverRaceResult): DriverRaceResult =>
  result.endPosition === -1 ? { ...result, endPosition: 10000000 } : result;

export const showTableHeading = (
  drFilter: DRFilters_enum,
  etFilter: string,
  driversToDisplay: FilterDisplayInfo[],
  racesToDisplay: FilterDisplayInfo[]
): string => {
  switch (drFilter) {
    case DRFilters_enum.FILTER_DRIVERS:
      if (etFilter === '') {
        return 'Standings';
      } else {
        const selectedDriver = driversToDisplay.find(
          (driver: FilterDisplayInfo) => driver.id === etFilter
        );
        return selectedDriver ? `Driver Results: \n ${selectedDriver.name}` : 'UNKNOWN';
      }
    case DRFilters_enum.FILTER_RACES:
      if (etFilter === '') {
        return 'Races';
      } else {
        return `Race Results for: ${
          racesToDisplay.find((race: FilterDisplayInfo) => race.id === etFilter)?.name || 'unknown'
        }`;
      }
    default:
      return 'Loading Data...';
  }
};

export const extractNeededDriverInfosFromRecord = (
  driversRecord: Record<string, F1Driver>
): FilterDisplayInfo[] =>
  Object.entries(driversRecord).map(([id, driver]: [string, F1Driver]) => ({
    id,
    name: `${driver.name} ${driver.surename}`,
    carNumber: driver.carNumber,
  }));

export const extractRacesFromRecord = (racesRecord: Record<string, Race>): FilterDisplayInfo[] =>
  Object.entries(racesRecord).map(([id, race]: [string, Race]) => ({
    id,
    name: race.circuit,
  }));

export const leagueDataBy = (
  [drFilter, entityFilter]: [DRFilters_enum, string],
  standings$: Observable<LeagueInfoDriversAll[]>,
  driversSingle$: Observable<LeagueInfoDriversSingle[]>,
  racesAll$: Observable<LeagueInfoRacesAll[]>,
  racesSingle$: Observable<LeagueInfoRacesSingle[]>
): Observable<LeagueData> => {
  switch (drFilter) {
    case DRFilters_enum.FILTER_DRIVERS:
      if (entityFilter === '') {
        return standings$;
      } else {
        return driversSingle$;
      }
    case DRFilters_enum.FILTER_RACES:
      if (entityFilter === '') {
        return racesAll$;
      } else {
        return racesSingle$;
      }
    default:
      return NEVER;
  }
};
