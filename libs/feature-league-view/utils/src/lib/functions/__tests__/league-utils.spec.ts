import {
  DRFilters_enum,
  getFastestLap,
  mockDriversToDisplay,
  mockRaceResult,
  mockRacesToDisplay,
  removeIrregularPosition,
  showTableHeading,
} from '@league-view/utils';

describe('LeagueUtils', () => {
  describe('get fastest lap', () => {
    const racesMock = mockRaceResult;

    it('should return the fastestLapTime from the driverResults', () => {
      const expected = 103.38025;

      const result = getFastestLap(racesMock);

      expect(result).toEqual(expected);
    });
  });

  describe('remove irregular position', () => {
    const racesMock = mockRaceResult;

    it('should remove the irregular position from the driverResult', () => {
      const driverResult = racesMock.driverResults[racesMock.driverResults.length - 1];
      const expected = 10000000;

      const result = removeIrregularPosition(driverResult).endPosition;

      expect(result).toEqual(expected);
    });
  });

  describe('show table heading', () => {
    const racesToDisplay = mockRacesToDisplay;
    const driversToDisplay = mockDriversToDisplay;

    describe('standings', () => {
      it('should show Standings as heading', () => {
        const expected = 'Standings';

        const result = showTableHeading(
          DRFilters_enum.FILTER_DRIVERS,
          '',
          driversToDisplay,
          racesToDisplay
        );

        expect(result).toEqual(expected);
      });
    });

    describe('Driver Single', () => {
      it('should show Driver Results: with driver name and car number', () => {
        const expected = 'Driver Results: \n Weber Robin';

        const result = showTableHeading(
          DRFilters_enum.FILTER_DRIVERS,
          '3b93addf-2cab-467e-852b-1881596a943c',
          driversToDisplay,
          racesToDisplay
        );

        expect(result).toEqual(expected);
      });
    });

    describe('Races All', () => {
      it('should return Races', () => {
        const expected = 'Races';

        const result = showTableHeading(
          DRFilters_enum.FILTER_RACES,
          '',
          driversToDisplay,
          racesToDisplay
        );

        expect(result).toEqual(expected);
      });

      it('should return race with according information', () => {
        const expected = 'Race Results for: CANADA';

        const result = showTableHeading(
          DRFilters_enum.FILTER_RACES,
          '58c48a6e-4b11-418b-9220-e757fbda3289',
          driversToDisplay,
          racesToDisplay
        );

        expect(result).toEqual(expected);
      });

      it('should return race with unknown for undefined id', () => {
        const expected = 'Race Results for: unknown';

        const result = showTableHeading(
          DRFilters_enum.FILTER_RACES,
          '58c48a6e-2344-418b-9220-e757fbda3289',
          driversToDisplay,
          racesToDisplay
        );

        expect(result).toEqual(expected);
      });
    });
  });
});
