import {
  LeagueInfoDriversAll,
  LeagueInfoDriversSingle,
  mockDriverByDriverId,
  mockDriverResult,
  mockRaceResults,
  mockRacesByRaceId,
  mockStandings,
  mockStandingsRow,
  mockUnfinishedDriverResult,
  toSingleDriverResult,
  toStandings,
} from '@league-view/utils';

describe('Data Converters', () => {
  const driverByDriverId = mockDriverByDriverId;
  const racesById = mockRacesByRaceId;
  const raceResults = mockRaceResults;

  describe('toStandings', () => {
    it('should return a full standing for driversById and standing rows', () => {
      const standingsRow = mockStandingsRow;
      const expected: LeagueInfoDriversAll[] = mockStandings;

      const result = toStandings([driverByDriverId, standingsRow]);

      expect(result).toHaveLength(2);
      expect(result).toEqual(expected);
    });
  });

  describe('toSingleDriverResult', () => {
    it('should return singleDriverResults for a finished race', () => {
      const mockEtFilter = '3b93addf-2cab-467e-852b-1881596a943c';
      const expected: LeagueInfoDriversSingle[] = mockDriverResult;

      const result = toSingleDriverResult([mockEtFilter, raceResults, driverByDriverId, racesById]);

      expect(result).toHaveLength(1);
      expect(result).toEqual(expected);
    });

    it('should return an unfinished Result for an unfinished RaceResult', () => {
      const mockEtFilter = '4af87ef8-40aa-4cad-a009-65c312d6c46f';
      const expected: LeagueInfoDriversSingle[] = mockUnfinishedDriverResult;

      const result = toSingleDriverResult([mockEtFilter, raceResults, driverByDriverId, racesById]);

      expect(result).toEqual(expected);
    });

    it('should skip undefined driver results', () => {
      const mockEtFilter = '4af87ef8-40aa-4cad-a009-65c312d6d45r';

      const result = toSingleDriverResult([mockEtFilter, raceResults, driverByDriverId, racesById]);

      expect(result).toHaveLength(0);
      expect(result).toEqual([]);
    });
  });

  describe('toRacesAll', () => {
    test.todo('implement');
  });

  describe('toRacesSingle', () => {
    test.todo('implement');
  });
});
