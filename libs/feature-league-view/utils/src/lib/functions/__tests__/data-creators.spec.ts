import { createFullLeague } from '../data-creators';
import { creatorLeagueMock, FullLeague, shortRacesMock } from '@league-view/utils';

describe('createFullLeague', () => {
  let fullLeague: FullLeague;

  beforeEach(() => {
    fullLeague = createFullLeague(creatorLeagueMock, shortRacesMock);
  });

  it('should have the correct leagueName', () => {
    expect(fullLeague.leagueName).toEqual(creatorLeagueMock.name);
  });

  it('should have the correct driverRecord', () => {
    expect(Object.keys(fullLeague.driversByDriverId)).toHaveLength(2);
    expect(fullLeague.driversByDriverId[creatorLeagueMock.drivers[0].driverId]).toEqual(
      creatorLeagueMock.drivers[0]
    );
    expect(fullLeague.driversByDriverId[creatorLeagueMock.drivers[1].driverId]).toEqual(
      creatorLeagueMock.drivers[1]
    );
  });

  it('should have the correct raceRecord', () => {
    expect(Object.keys(fullLeague.racesByRaceId)).toHaveLength(2);
    expect(fullLeague.racesByRaceId[shortRacesMock[0].raceId]).toEqual(shortRacesMock[0]);
    expect(fullLeague.racesByRaceId[shortRacesMock[1].raceId]).toEqual(shortRacesMock[1]);
  });
});
