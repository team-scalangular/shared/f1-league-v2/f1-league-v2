import { Race } from '@generated/openapi';

export const leagueViewMocks = {
  name: 'f1 2021 event',
  leagueId: '4ccc5a4f-bdcb-4bd5-a7e8-2382adf804e2',
  drivers: [
    {
      name: 'Weber',
      driverId: '3b93addf-2cab-467e-852b-1881596a943c',
      surename: 'Robin',
      carNumber: 27,
    },
    {
      name: 'Damm',
      driverId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      surename: 'Alexander',
      carNumber: 96,
    },
    {
      name: 'Damm',
      driverId: 'e1bfa40d-00a4-43bc-8b1c-6ca7127bcc34',
      surename: 'Marco',
      carNumber: 9,
    },
    {
      name: 'Bieleke',
      driverId: '7b276df0-ff99-4082-acda-830136a20e7a',
      surename: 'Maximilian',
      carNumber: 12,
    },
    {
      name: 'Gros',
      driverId: '00c59e51-d00d-437e-bfe0-dfa4276c2337',
      surename: 'Max',
      carNumber: 75,
    },
    {
      name: 'Schellenberg',
      driverId: '8b38ae3a-4c51-44e2-b79f-da35a9f320c3',
      surename: 'Leon',
      carNumber: 68,
    },
    {
      name: 'Baur',
      driverId: '8bc47d29-aed8-4b1e-810a-de461736a959',
      surename: 'Bastian',
      carNumber: 25,
    },
    {
      name: 'Lindner',
      driverId: '2f3151dc-5156-450d-891e-d8955e387262',
      surename: 'Moritz',
      carNumber: 21,
    },
    {
      name: 'Baur',
      driverId: '887d1271-ff15-4859-9c5c-1fc08fa1cc81',
      surename: 'Fabian',
      carNumber: 47,
    },
    {
      name: 'Kmoch',
      driverId: '3d856784-e521-4253-8f74-54b4efd0a21b',
      surename: 'Jesse',
      carNumber: 43,
    },
    {
      name: 'Kamprath',
      driverId: '307c0ce3-1609-4713-b339-b2a98f1ffe58',
      surename: 'Jonas',
      carNumber: 64,
    },
    {
      name: 'Weber',
      driverId: 'c62d203c-aa54-4e3e-a561-9be98fec0f69',
      surename: 'Frank',
      carNumber: 19,
    },
    {
      name: 'Heimer',
      driverId: 'bc6ec418-26dc-41e7-a10a-3bf8f5e38f59',
      surename: 'Mark Alexander',
      carNumber: 13,
    },
    {
      name: 'Eckhardt',
      driverId: '534d5c7b-927c-487b-87cc-0ff10389382f',
      surename: 'Marco',
      carNumber: 36,
    },
    {
      name: 'Reinig',
      driverId: '3d958cf0-6426-4b00-bb0d-ada6854b33dd',
      surename: 'Philipp',
      carNumber: 50,
    },
    {
      name: 'Baur',
      driverId: '940795cc-ff4c-459b-bd6e-db2e293bbd60',
      surename: 'Sabine',
      carNumber: 69,
    },
    {
      name: 'Baur',
      driverId: '226826a5-390b-4fd6-aaf8-7f584bb38992',
      surename: 'Adrian',
      carNumber: 66,
    },
  ],
};

export const standingsMock = [
  {
    driverId: '7b276df0-ff99-4082-acda-830136a20e7a',
    position: 1,
    points: 135,
  },
  {
    driverId: '3b93addf-2cab-467e-852b-1881596a943c',
    position: 2,
    points: 133,
  },
  {
    driverId: '2f3151dc-5156-450d-891e-d8955e387262',
    position: 3,
    points: 127,
  },
  {
    driverId: '8bc47d29-aed8-4b1e-810a-de461736a959',
    position: 4,
    points: 110,
  },
  { driverId: '887d1271-ff15-4859-9c5c-1fc08fa1cc81', position: 5, points: 94 },
  { driverId: '226826a5-390b-4fd6-aaf8-7f584bb38992', position: 6, points: 86 },
  { driverId: 'c62d203c-aa54-4e3e-a561-9be98fec0f69', position: 7, points: 85 },
  { driverId: '307c0ce3-1609-4713-b339-b2a98f1ffe58', position: 8, points: 29 },
  { driverId: 'bc6ec418-26dc-41e7-a10a-3bf8f5e38f59', position: 9, points: 26 },
  {
    driverId: 'e1bfa40d-00a4-43bc-8b1c-6ca7127bcc34',
    position: 10,
    points: 23,
  },
  {
    driverId: '8b38ae3a-4c51-44e2-b79f-da35a9f320c3',
    position: 11,
    points: 20,
  },
  {
    driverId: '534d5c7b-927c-487b-87cc-0ff10389382f',
    position: 12,
    points: 19,
  },
  {
    driverId: '3d958cf0-6426-4b00-bb0d-ada6854b33dd',
    position: 13,
    points: 18,
  },
  {
    driverId: '00c59e51-d00d-437e-bfe0-dfa4276c2337',
    position: 14,
    points: 17,
  },
  { driverId: '940795cc-ff4c-459b-bd6e-db2e293bbd60', position: 15, points: 0 },
];

export const racesMock: Race[] = [
  {
    circuit: 'BAHRAIN',
    raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
    racePositionIndex: 0,
    date: '2021-05-11',
  },
  {
    circuit: 'CANADA',
    raceId: '58c48a6e-4b11-418b-9220-e757fbda3289',
    racePositionIndex: 1,
    date: '2021-05-11',
  },
  {
    circuit: 'MONACO',
    raceId: '9094430e-302c-4314-b543-3c891866338e',
    racePositionIndex: 2,
    date: '2021-05-11',
  },
  {
    circuit: 'HUNGARY',
    raceId: '7755fbe4-e983-49b2-89e6-e9128f03c35e',
    racePositionIndex: 3,
    date: '2021-06-01',
  },
  {
    circuit: 'AZERBAIJAN',
    raceId: 'e07f8b2f-aaba-4a38-8552-c6d8340f271f',
    racePositionIndex: 4,
    date: '2021-06-01',
  },
  {
    circuit: 'BELGIUM',
    raceId: '6d38eb1c-5ad9-4dab-86ae-9481bd63377f',
    racePositionIndex: 5,
    date: '2021-06-01',
  },
  {
    circuit: 'ENGLAND',
    raceId: '5fd9a525-f8bb-4fbc-b4c5-6257f8ee4293',
    racePositionIndex: 6,
    date: '2021-07-06',
  },
  {
    circuit: 'VIETNAM',
    raceId: 'ec1108d5-84f4-4297-b1bd-7b5ce8b25b16',
    racePositionIndex: 8,
    date: '2021-07-06',
  },
  {
    circuit: 'MEXICO',
    raceId: '487cc0eb-80c0-40a0-8e07-e8eb96daab93',
    racePositionIndex: 9,
    date: '2021-08-03',
  },
  {
    circuit: 'CHINA',
    raceId: 'a709f2c5-acbf-4c7f-b83b-f6bbf96fbfd7',
    racePositionIndex: 10,
    date: '2021-08-03',
  },
  {
    circuit: 'RUSSIA',
    raceId: '56ef3ba0-3535-4c9a-8252-40cd1f4d310e',
    racePositionIndex: 11,
    date: '2021-08-03',
  },
  {
    circuit: 'FRANCE',
    raceId: '719fa254-ad55-4d84-acff-182b5114cd66',
    racePositionIndex: 13,
    date: '2021-09-07',
  },
  {
    circuit: 'NETHERLANDS',
    raceId: '953b1fe7-9500-49fa-b4dd-b6278300322c',
    racePositionIndex: 12,
    date: '2021-09-07',
  },
  {
    circuit: 'AUSTRALIA',
    raceId: 'd7df67ad-7408-46d0-96ef-f7e9634a37a0',
    racePositionIndex: 14,
    date: '2021-09-07',
  },
  {
    circuit: 'AUSTRIA',
    raceId: 'd313fc61-120e-4f8c-80f3-a7a20f3d3d84',
    racePositionIndex: 17,
    date: '2021-10-05',
  },
  {
    circuit: 'JAPAN',
    raceId: 'f023ce74-8766-4ba5-ae0e-ca74147359c3',
    racePositionIndex: 16,
    date: '2021-10-05',
  },
  {
    circuit: 'SPAIN',
    raceId: 'c4b56d53-37fb-4760-89b8-ee8ba677a882',
    racePositionIndex: 15,
    date: '2021-10-05',
  },
  {
    circuit: 'SINGAPORE',
    raceId: 'b68bad12-4fc8-41fb-9223-10e8601dfa56',
    racePositionIndex: 18,
    date: '2021-11-02',
  },
  {
    circuit: 'ITALY',
    raceId: 'a7049fe2-d6e9-4d29-a5c9-7188eb7e9ccd',
    racePositionIndex: 19,
    date: '2021-11-02',
  },
  {
    circuit: 'USA',
    raceId: '0fbc3e97-6e37-41a8-8240-f53cc95e7461',
    racePositionIndex: 20,
    date: '2021-11-02',
  },
  {
    circuit: 'BRASIL',
    raceId: '1301a58d-b867-4aff-b9c0-1e6f7938719c',
    racePositionIndex: 21,
    date: '2021-11-02',
  },
];
