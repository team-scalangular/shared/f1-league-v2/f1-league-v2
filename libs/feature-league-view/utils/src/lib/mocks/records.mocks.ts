import { F1Circuit, F1Driver, League, Race, RaceResult, StandingRow } from '@generated/openapi';
import { LeagueInfoDriversAll, LeagueInfoDriversSingle } from '../interfaces/table';

export const shortLeagueMock: League = {
  name: 'f1 2021 event',
  leagueId: '4ccc5a4f-bdcb-4bd5-a7e8-2382adf804e2',
  drivers: [
    {
      name: 'Weber',
      driverId: '3b93addf-2cab-467e-852b-1881596a943c',
      surename: 'Robin',
      carNumber: 27,
    },
    {
      name: 'Damm',
      driverId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      surename: 'Alexander',
      carNumber: 96,
    },
  ],
};

export const shorRacesMock: Race[] = [
  {
    circuit: 'BAHRAIN',
    raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
    racePositionIndex: 0,
    date: '2021-05-11',
  },
  {
    circuit: 'CANADA',
    raceId: '58c48a6e-4b11-418b-9220-e757fbda3289',
    racePositionIndex: 1,
    date: '2021-05-11',
  },
];

export const mockDriverByDriverId: Record<string, F1Driver> = {
  '3b93addf-2cab-467e-852b-1881596a943c': {
    name: 'Weber',
    driverId: '3b93addf-2cab-467e-852b-1881596a943c',
    surename: 'Robin',
    carNumber: 27,
  },
  '4af87ef8-40aa-4cad-a009-65c312d6c46f': {
    name: 'Damm',
    driverId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
    surename: 'Alexander',
    carNumber: 96,
  },
};

export const mockRacesByRaceId: Record<string, Race> = {
  '4af87ef8-40aa-4cad-a009-65c312d6c46f': {
    circuit: 'BAHRAIN',
    raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
    racePositionIndex: 0,
    date: '2021-05-11',
  },
  '58c48a6e-4b11-418b-9220-e757fbda3289': {
    circuit: 'CANADA',
    raceId: '58c48a6e-4b11-418b-9220-e757fbda3289',
    racePositionIndex: 1,
    date: '2021-05-11',
  },
};

export const mockStandingsRow: StandingRow[] = [
  { driverId: '3b93addf-2cab-467e-852b-1881596a943c', position: 1, points: 25 },
  { driverId: '4af87ef8-40aa-4cad-a009-65c312d6c46f', position: 2, points: 18 },
];

export const mockRaceResults: RaceResult[] = [
  {
    raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
    driverResults: [
      {
        raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
        endPosition: 1,
        drivenLaps: 13,
        gridPosition: 1,
        bestLapTime: 103.38025,
        totalRaceTime: 1390.8109130859375,
        penaltiesAmount: 0,
        penaltiesTime: 0,
        pitStops: 1,
        driverId: '3b93addf-2cab-467e-852b-1881596a943c',
        raceNumber: 1,
        raceResult: 'finished',
      },
      {
        raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
        endPosition: -1,
        drivenLaps: -1,
        gridPosition: -1,
        bestLapTime: -1,
        totalRaceTime: -1,
        penaltiesAmount: -1,
        penaltiesTime: -1,
        pitStops: -1,
        driverId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
        raceNumber: 96,
        raceResult: 'notClassified',
      },
    ],
  },
];

export const mockStandings: LeagueInfoDriversAll[] = [
  { position: 1, fullName: 'Weber Robin', carNumber: 27, points: 25 },
  { position: 2, fullName: 'Damm Alexander', carNumber: 96, points: 18 },
];

export const mockDriverResult: LeagueInfoDriversSingle[] = [
  {
    grandPrix: F1Circuit.Bahrain,
    date: '2021-05-11',
    endPosition: 1,
    gridPosition: 1,
    pitStops: 1,
    fastestLap: 103.38025,
    totalTime: 1390.8109130859375,
    penaltyAmount: 0,
    penaltyTime: 0,
    raceResult: 'finished',
    points: 26,
    isFastestLap: true,
  },
];
export const mockUnfinishedDriverResult: LeagueInfoDriversSingle[] = [
  {
    grandPrix: F1Circuit.Bahrain,
    date: '2021-05-11',
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    endPosition: 'DNS',
    gridPosition: '---',
    pitStops: '---',
    fastestLap: '---',
    totalTime: '---',
    penaltyAmount: '---',
    penaltyTime: '---',
    raceResult: 'notClassified',
    points: 0,
    isFastestLap: false,
  },
];
