import { RaceResult } from '@generated/openapi';
import { FilterDisplayInfo } from '../interfaces/filter';

export const mockRaceResult: RaceResult = {
  raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
  driverResults: [
    {
      raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      endPosition: 4,
      drivenLaps: 13,
      gridPosition: 3,
      bestLapTime: 105.34354,
      totalRaceTime: 1410.565185546875,
      penaltiesAmount: 0,
      penaltiesTime: 0,
      pitStops: 1,
      driverId: '2f3151dc-5156-450d-891e-d8955e387262',
      raceNumber: 4,
      raceResult: 'finished',
    },
    {
      raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      endPosition: 7,
      drivenLaps: 13,
      gridPosition: 5,
      bestLapTime: 105.34393,
      totalRaceTime: 1432.8526611328125,
      penaltiesAmount: 0,
      penaltiesTime: 0,
      pitStops: 1,
      driverId: 'c62d203c-aa54-4e3e-a561-9be98fec0f69',
      raceNumber: 7,
      raceResult: 'finished',
    },
    {
      raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      endPosition: 3,
      drivenLaps: 13,
      gridPosition: 4,
      bestLapTime: 105.089455,
      totalRaceTime: 1407.78662109375,
      penaltiesAmount: 0,
      penaltiesTime: 0,
      pitStops: 1,
      driverId: '7b276df0-ff99-4082-acda-830136a20e7a',
      raceNumber: 2,
      raceResult: 'finished',
    },
    {
      raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      endPosition: 1,
      drivenLaps: 13,
      gridPosition: 1,
      bestLapTime: 103.38025,
      totalRaceTime: 1390.8109130859375,
      penaltiesAmount: 0,
      penaltiesTime: 0,
      pitStops: 1,
      driverId: '3b93addf-2cab-467e-852b-1881596a943c',
      raceNumber: 1,
      raceResult: 'finished',
    },
    {
      raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      endPosition: 5,
      drivenLaps: 13,
      gridPosition: 6,
      bestLapTime: 106.22235,
      totalRaceTime: 1410.61376953125,
      penaltiesAmount: 0,
      penaltiesTime: 0,
      pitStops: 1,
      driverId: '307c0ce3-1609-4713-b339-b2a98f1ffe58',
      raceNumber: 8,
      raceResult: 'finished',
    },
    {
      raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      endPosition: 8,
      drivenLaps: 13,
      gridPosition: 9,
      bestLapTime: 107.11267,
      totalRaceTime: 1436.2958984375,
      penaltiesAmount: 0,
      penaltiesTime: 0,
      pitStops: 1,
      driverId: '226826a5-390b-4fd6-aaf8-7f584bb38992',
      raceNumber: 6,
      raceResult: 'finished',
    },
    {
      raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      endPosition: 10,
      drivenLaps: 13,
      gridPosition: 11,
      bestLapTime: 106.44153,
      totalRaceTime: 1495.4559326171875,
      penaltiesAmount: 1,
      penaltiesTime: 3,
      pitStops: 1,
      driverId: '534d5c7b-927c-487b-87cc-0ff10389382f',
      raceNumber: 12,
      raceResult: 'finished',
    },
    {
      raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      endPosition: 12,
      drivenLaps: 11,
      gridPosition: 12,
      bestLapTime: 126.6622,
      totalRaceTime: 1520.9052734375,
      penaltiesAmount: 1,
      penaltiesTime: 3,
      pitStops: 1,
      driverId: '00c59e51-d00d-437e-bfe0-dfa4276c2337',
      raceNumber: 13,
      raceResult: 'finished',
    },
    {
      raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      endPosition: -1,
      drivenLaps: -1,
      gridPosition: -1,
      bestLapTime: -1,
      totalRaceTime: -1,
      penaltiesAmount: -1,
      penaltiesTime: -1,
      pitStops: -1,
      driverId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      raceNumber: 96,
      raceResult: 'notClassified',
    },
  ],
};

export const mockDriversToDisplay: FilterDisplayInfo[] = [
  { id: '3b93addf-2cab-467e-852b-1881596a943c', name: 'Weber Robin', carNumber: 27 },
  { id: '4af87ef8-40aa-4cad-a009-65c312d6c46f', name: 'Damm Alexander', carNumber: 96 },
  { id: 'e1bfa40d-00a4-43bc-8b1c-6ca7127bcc34', name: 'Damm Marco', carNumber: 9 },
  { id: '7b276df0-ff99-4082-acda-830136a20e7a', name: 'Bieleke Maximilian', carNumber: 12 },
  { id: '00c59e51-d00d-437e-bfe0-dfa4276c2337', name: 'Gros Max' },
];

export const mockRacesToDisplay: FilterDisplayInfo[] = [
  { id: '4af87ef8-40aa-4cad-a009-65c312d6c46f', name: 'BAHRAIN' },
  { id: '58c48a6e-4b11-418b-9220-e757fbda3289', name: 'CANADA' },
  { id: '9094430e-302c-4314-b543-3c891866338e', name: 'MONACO' },
  { id: '7755fbe4-e983-49b2-89e6-e9128f03c35e', name: 'HUNGARY' },
];
