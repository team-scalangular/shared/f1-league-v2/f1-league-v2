import { League, Race } from '@generated/openapi';

export const creatorLeagueMock: League = {
  name: 'f1 2021 event',
  leagueId: '4ccc5a4f-bdcb-4bd5-a7e8-2382adf804e2',
  drivers: [
    {
      name: 'Weber',
      driverId: '3b93addf-2cab-467e-852b-1881596a943c',
      surename: 'Robin',
      carNumber: 27,
    },
    {
      name: 'Damm',
      driverId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
      surename: 'Alexander',
      carNumber: 96,
    },
  ],
};

export const shortRacesMock: Race[] = [
  {
    circuit: 'BAHRAIN',
    raceId: '4af87ef8-40aa-4cad-a009-65c312d6c46f',
    racePositionIndex: 0,
    date: '2021-05-11',
  },
  {
    circuit: 'CANADA',
    raceId: '58c48a6e-4b11-418b-9220-e757fbda3289',
    racePositionIndex: 1,
    date: '2021-05-11',
  },
];
