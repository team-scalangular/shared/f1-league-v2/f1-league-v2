import { DriverRaceResult, F1Driver, Race } from '@generated/openapi';
import {
  calculatePointsBy,
  LeagueInfoDriversSingle,
  LeagueInfoRacesAll,
  LeagueInfoRacesSingle,
  raceResultByEnum,
} from '@league-view/utils';

export const unfinishedDriverRaceResult = (
  race: Race,
  driverResult: DriverRaceResult
): LeagueInfoDriversSingle =>
  ({
    grandPrix: race.circuit,
    date: race.date,
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    endPosition: raceResultByEnum[driverResult.raceResult!] || 'UNKNOWN',
    gridPosition: '---',
    pitStops: '---',
    fastestLap: '---',
    totalTime: '---',
    penaltyAmount: '---',
    penaltyTime: '---',
    raceResult: driverResult.raceResult,
    points: 0,
    isFastestLap: false,
  } as LeagueInfoDriversSingle);

export const completeDriverRaceResult = (
  race: Race,
  driverResult: DriverRaceResult,
  isFastestLap: boolean
) =>
  ({
    grandPrix: race.circuit,
    date: race.date,
    endPosition: driverResult.endPosition,
    gridPosition: driverResult.gridPosition,
    pitStops: driverResult.pitStops,
    fastestLap: driverResult.bestLapTime,
    totalTime: driverResult.totalRaceTime,
    penaltyAmount: driverResult.penaltiesAmount,
    penaltyTime: driverResult.penaltiesTime,
    raceResult: driverResult.raceResult,
    points: calculatePointsBy(driverResult.endPosition, isFastestLap),
    isFastestLap: isFastestLap,
  } as LeagueInfoDriversSingle);

export const getCompleteRace = (
  race: Race,
  driverById: Record<string, F1Driver>,
  winner: DriverRaceResult
): LeagueInfoRacesAll =>
  ({
    grandPrix: race.circuit,
    date: race.date,
    winner: `${driverById[winner.driverId].name} ${driverById[winner.driverId].surename}`,
    laps: winner.drivenLaps,
    raceTime: winner.totalRaceTime,
  } as LeagueInfoRacesAll);

export const getUnfinishedRaceResult = (race: Race): LeagueInfoRacesAll =>
  ({
    grandPrix: race.circuit,
    date: race.date || 'TBD.',
    winner: 'TBD.',
    laps: '---',
    raceTime: '---',
  } as LeagueInfoRacesAll);

export const unfinishedRaceResult = (
  result: DriverRaceResult,
  driversById: Record<string, F1Driver>
): LeagueInfoRacesSingle =>
  ({
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    position: raceResultByEnum[result.raceResult!] || 'UNKNOWN',
    number: driversById[result.driverId].carNumber,
    driver: `${driversById[result.driverId].name} ${driversById[result.driverId].surename}`,
    gridPosition: '---',
    pitStops: '---',
    fastestLap: '---',
    totalTime: '---',
    penaltyAmount: '---',
    penaltyTime: '---',
    laps: '---',
    points: 0,
    raceResult: result.raceResult,
    isFastestLap: false,
  } as LeagueInfoRacesSingle);

export const completeRaceResult = (
  result: DriverRaceResult,
  driversById: Record<string, F1Driver>,
  isFastestLap: boolean
): LeagueInfoRacesSingle =>
  ({
    position: result.endPosition,
    number: driversById[result.driverId].carNumber,
    driver: `${driversById[result.driverId].name} ${driversById[result.driverId].surename}`,
    gridPosition: result.gridPosition,
    pitStops: result.pitStops,
    fastestLap: result.bestLapTime,
    totalTime: result.totalRaceTime,
    penaltyAmount: result.penaltiesAmount,
    penaltyTime: result.penaltiesTime,
    laps: result.drivenLaps,
    points: calculatePointsBy(result.endPosition, isFastestLap),
    raceResult: result.raceResult,
    isFastestLap: isFastestLap,
  } as LeagueInfoRacesSingle);
