import { F1RaceResult } from '@generated/openapi';
import {
  TableData,
  TableData_Drivers_All,
  TableData_Drivers_Single,
  TableData_enum,
  TableData_Races_All,
  TableData_Races_Single,
} from '../interfaces/table';

// static folder for static maps, functions etc.
export const pointsByEndPosition: Record<number, number> = {
  1: 25,
  2: 18,
  3: 15,
  4: 12,
  5: 10,
  6: 8,
  7: 6,
  8: 4,
  9: 2,
  10: 1,
};

export const raceResultByEnum: Record<string, string> = {
  [F1RaceResult.Disqualified]: 'DSQ',
  [F1RaceResult.Retired]: 'DNF',
  [F1RaceResult.NotClassified]: 'DNS',
};

export const TableDataTypesByTableDataEnum: Record<TableData_enum, TableData> = {
  [TableData_enum.DRIVERS_ALL]: [
    { field: 'position', header: 'Position' },
    { field: 'fullName', header: 'Name' },
    { field: 'carNumber', header: 'Car Number' },
    { field: 'points', header: 'Points' },
  ] as TableData_Drivers_All[],
  [TableData_enum.DRIVERS_SINGLE]: [
    { field: 'grandPrix', header: 'Grand Prix' },
    { field: 'date', header: 'Date' },
    { field: 'endPosition', header: 'End Position' },
    { field: 'gridPosition', header: 'Grid Position' },
    { field: 'pitStops', header: 'Pit Stops' },
    { field: 'fastestLap', header: 'Fastest Lap' },
    { field: 'totalTime', header: 'Total Time' },
    { field: 'penaltyAmount', header: 'Penalties Amount' },
    { field: 'penaltyTime', header: 'Penalty Time' },
    { field: 'points', header: 'Points' },
  ] as TableData_Drivers_Single[],
  [TableData_enum.RACES_ALL]: [
    { field: 'grandPrix', header: 'Grand Prix' },
    { field: 'date', header: 'Date' },
    { field: 'winner', header: 'Winner' },
    { field: 'laps', header: 'Laps' },
    { field: 'raceTime', header: 'Race Time' },
  ] as TableData_Races_All[],
  [TableData_enum.RACES_SINGLE]: [
    { field: 'position', header: 'Position' },
    { field: 'number', header: 'Number' },
    { field: 'driver', header: 'Driver' },
    { field: 'gridPosition', header: 'Grid Position' },
    { field: 'pitStops', header: 'Pit Stops' },
    { field: 'fastestLap', header: 'Fastest Lap' },
    { field: 'totalTime', header: 'Total Time' },
    { field: 'penaltyAmount', header: 'Penalties Amount' },
    { field: 'penaltyTime', header: 'Penalty Time' },
    { field: 'laps', header: 'Laps' },
    { field: 'points', header: 'Points' },
  ] as TableData_Races_Single[],
};
