import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { LEAGUE_AUTHENTICATION_FEATURE_KEY, leagueAuthReducers } from './state';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './state/auth/auth.effects';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/auth.interceptor';
import { MessageService } from 'primeng/api';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(LEAGUE_AUTHENTICATION_FEATURE_KEY, leagueAuthReducers),
    EffectsModule.forFeature([AuthEffects]),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    MessageService,
  ],
})
export class FeatureAuthDataAccessModule {}
