import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { DefaultService, RaceResult } from '@generated/openapi';
import { MessageService } from 'primeng/api';
import { CallState, ErrorState } from '@libs/utils';

export interface FileUploadMetaData {
  fileText: string;
  raceId: string;
  leagueId: string;
}

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  public fileUploadRequest$: Subject<FileUploadMetaData> = new Subject<FileUploadMetaData>();
  public fileUploadObserver$: Observable<boolean> = this.fileUploadRequest$.pipe(
    mergeMap(this.upload.bind(this)),
    map((result: RaceResult | CallState) => {
      return (result as RaceResult).raceId !== undefined
        ? this.handleSuccess(result as RaceResult)
        : this.handleError(result as ErrorState);
    })
  );

  constructor(private defaultService: DefaultService, private messageService: MessageService) {}

  private handleError(result: ErrorState): boolean {
    this.messageService.add({
      severity: 'error',
      summary: 'An Error Occured!',
      detail: `${result.errorMessage}`,
      life: 5000,
    });
    return false;
  }

  private handleSuccess(result: RaceResult): boolean {
    this.messageService.add({
      severity: 'success',
      summary: 'Upload Success!',
      detail: `The RaceResults JSON has been uploaded to race with Id: ${result.raceId}`,
      life: 5000,
    });
    return true;
  }

  private upload(fileUploadMetaData: FileUploadMetaData): Observable<RaceResult | CallState> {
    return this.defaultService
      .leagueLeagueIdRaceresultRaceIdRacePost(
        fileUploadMetaData.leagueId,
        fileUploadMetaData.raceId,
        fileUploadMetaData.fileText
      )
      .pipe(catchError((err) => of({ errorMessage: err.error })));
  }
}
