import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TokenStorageRefService {
  set token(token: string) {
    localStorage.setItem('refreshToken', token);
  }
  get token(): string {
    return localStorage.getItem('refreshToken') || '';
  }
}
