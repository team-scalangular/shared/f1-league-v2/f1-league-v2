import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthFacade } from '@libs/auth/data-access';
import { mergeMap, switchMap, take } from 'rxjs/operators';

function requestShouldBeIntercepted(refreshToken: string, req: HttpRequest<unknown>): boolean {
  return !!refreshToken && req.method === 'POST' && !req.url.includes('auth');
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authFacade.token$.pipe(
      take(1),
      mergeMap((refreshToken: string) => {
        return requestShouldBeIntercepted(refreshToken, request)
          ? this.addAccessTokenTo(next, request)
          : next.handle(request);
      })
    );
  }

  private addAccessTokenTo(next: HttpHandler, request: HttpRequest<unknown>) {
    return this.authFacade.getAccessToken().pipe(
      switchMap((accessToken: string) =>
        next.handle(
          request.clone({
            setHeaders: { Authorization: `Bearer ${accessToken}` },
          })
        )
      )
    );
  }

  constructor(private authFacade: AuthFacade) {}
}
