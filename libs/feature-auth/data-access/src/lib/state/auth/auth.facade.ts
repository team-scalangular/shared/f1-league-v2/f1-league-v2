import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { authenticationError, isLoggedIn, loading, selectToken } from './auth.selectors';
import { AuthActions } from './auth-types';
import { filter } from 'rxjs/operators';
import { notEmpty } from '@libs/utils';
import { Authapi2Service } from '@generated/authapi';
import { TokenStorageRefService } from '../../services/token-storage-ref.service';

@Injectable({
  providedIn: 'root',
})
export class AuthFacade {
  public token$: Observable<string> = this.store.select(selectToken);
  public isAuthenticated$: Observable<boolean> = this.store.select(isLoggedIn);
  public loading$: Observable<boolean> = this.store.select(loading);
  public error$: Observable<string> = this.store.select(authenticationError).pipe(filter(notEmpty));

  constructor(
    private store: Store,
    private authService: Authapi2Service,
    private tokenService: TokenStorageRefService
  ) {}

  public login(username: string, password: string): void {
    this.store.dispatch(
      AuthActions.startLogin({
        authenticationCredentials: {
          username,
          password,
        },
      })
    );
  }

  public logout(): void {
    this.store.dispatch(AuthActions.logout());
  }

  public dispatchValidationError(errorMessage: string): void {
    this.store.dispatch(
      AuthActions.authenticationError({
        errorMessage,
      })
    );
  }

  public getAccessToken(): Observable<string> {
    return this.authService.accessPost();
  }
}
