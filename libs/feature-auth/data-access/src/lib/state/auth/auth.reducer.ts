import { CallState, LoadingState } from '@libs/utils';
import { createReducer, on } from '@ngrx/store';
import { AuthActions } from './auth-types';

export const AUTH_FEATURE_KEY = 'auth';

export interface AuthState {
  authToken: string;
  callState: CallState;
}

export const initialAuthState: AuthState = {
  authToken: '',
  callState: LoadingState.INIT,
};

export const authReducer = createReducer(
  initialAuthState,
  on(AuthActions.authorizationInit, (state) => ({
    ...state,
    callState: LoadingState.LOADING,
  })),
  on(AuthActions.unauthorized, (state) => ({
    ...state,
    callState: LoadingState.INIT,
  })),
  on(AuthActions.startLogin, (state) => ({
    ...state,
    callState: LoadingState.LOADING,
  })),
  on(AuthActions.loginSuccess, (state, action) => ({
    ...state,
    authToken: action.authToken,
    callState: LoadingState.LOADED,
  })),
  on(AuthActions.authenticationError, (state, action) => ({
    ...state,
    callState: { errorMessage: action.errorMessage },
  })),
  on(AuthActions.logout, () => initialAuthState)
);
