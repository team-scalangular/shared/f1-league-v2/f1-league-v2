import { createSelector } from '@ngrx/store';
import { selectLeagueAuthState } from '../index';
import { AUTH_FEATURE_KEY } from './auth.reducer';
import { extractErrorIfPresent, isLoaded, isLoading } from '@libs/utils';

export const selectAuthState = createSelector(
  selectLeagueAuthState,
  (state) => state[AUTH_FEATURE_KEY]
);

export const selectToken = createSelector(selectAuthState, (state) => state.authToken);

export const loading = createSelector(selectAuthState, (state) => isLoading(state.callState));

export const isLoggedIn = createSelector(selectAuthState, (state) => isLoaded(state.callState));

export const authenticationError = createSelector(selectAuthState, (state) =>
  extractErrorIfPresent(state.callState)
);
