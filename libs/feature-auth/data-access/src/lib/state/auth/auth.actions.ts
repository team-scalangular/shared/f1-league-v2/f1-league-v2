import { createAction, props } from '@ngrx/store';
import { AuthenticationCredentials } from '@libs/auth/data-access';

export const authorizationInit = createAction('[Init] Authorization init');

export const unauthorized = createAction('[Init] Unauthorized');

export const startLogin = createAction(
  '[Overlay Panel] Start Authentication',
  props<{ authenticationCredentials: AuthenticationCredentials }>()
);

export const loginSuccess = createAction(
  '[Auth Effects] Login Succeeded',
  props<{ authToken: string }>()
);

export const authenticationError = createAction(
  '[Misc] Authentication Error Occurred',
  props<{ errorMessage: string }>()
);

export const logout = createAction('[Overlay Panel] Logout');
