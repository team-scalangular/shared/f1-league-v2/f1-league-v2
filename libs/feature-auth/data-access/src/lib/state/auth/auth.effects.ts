import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { AuthActions } from './auth-types';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Authapi2Service } from '@generated/authapi';
import { of } from 'rxjs';
import { Action } from '@ngrx/store';
import {
  AuthenticationCredentials,
  AuthFacade,
  TokenStorageRefService,
} from '@libs/auth/data-access';
import jwtDecode, { JwtPayload } from 'jwt-decode';
import { Either } from 'monet';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class AuthEffects implements OnInitEffects {
  validateStorageForToken$ = createEffect(() =>
    this.actions.pipe(
      ofType(AuthActions.authorizationInit),
      map(() => this.tokenReference.token),
      map((token: string) => {
        return Either.fromTry(() => jwtDecode<JwtPayload>(token)).fold(
          () => AuthActions.unauthorized(),
          (decodedToken: JwtPayload) => {
            const decodedExpirationDate = decodedToken.exp;
            const currentTime = Date.now().valueOf() / 1000;
            if (decodedExpirationDate && currentTime > decodedExpirationDate) {
              this.tokenReference.token = '';
              return AuthActions.unauthorized();
            }
            return AuthActions.loginSuccess({ authToken: token });
          }
        );
      })
    )
  );

  authenticationRequested$ = createEffect(() =>
    this.actions.pipe(
      ofType(AuthActions.startLogin),
      map((action) => action.authenticationCredentials),
      switchMap((authCredentials: AuthenticationCredentials) =>
        this.authService.authPost(authCredentials).pipe(
          map((authToken: string) => AuthActions.loginSuccess({ authToken })),
          catchError((error: HttpErrorResponse) =>
            of(AuthActions.authenticationError({ errorMessage: error.error }))
          )
        )
      )
    )
  );

  storeTokenOnLoginSuccess$ = createEffect(
    () =>
      this.actions.pipe(
        ofType(AuthActions.loginSuccess),
        map((action) => action.authToken),
        tap(this.storeRefreshToken.bind(this))
      ),
    { dispatch: false }
  );

  removeTokenOnLogout$ = createEffect(
    () =>
      this.actions.pipe(
        ofType(AuthActions.logout),
        tap(() => (this.tokenReference.token = ''))
      ),
    { dispatch: false }
  );

  constructor(
    private actions: Actions,
    private authService: Authapi2Service,
    private tokenReference: TokenStorageRefService,
    private authFacade: AuthFacade
  ) {}

  ngrxOnInitEffects(): Action {
    return AuthActions.authorizationInit();
  }

  private storeRefreshToken(refreshToken: string): void {
    this.authService.configuration.credentials.bearerAuth = refreshToken;
    this.tokenReference.token = refreshToken;
  }
}
