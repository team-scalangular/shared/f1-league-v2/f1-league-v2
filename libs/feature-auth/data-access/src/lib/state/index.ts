import { AUTH_FEATURE_KEY, authReducer, AuthState } from './auth/auth.reducer';
import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

export const LEAGUE_AUTHENTICATION_FEATURE_KEY = 'league-authentication';

export interface LeagueAuthState {
  [AUTH_FEATURE_KEY]: AuthState;
}

export const leagueAuthReducers: ActionReducerMap<LeagueAuthState> = {
  [AUTH_FEATURE_KEY]: authReducer,
};

export const selectLeagueAuthState = createFeatureSelector<LeagueAuthState>(
  LEAGUE_AUTHENTICATION_FEATURE_KEY
);
