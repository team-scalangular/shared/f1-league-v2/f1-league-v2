import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureAuthUiModule } from '@f1-league-v2/feature-auth/ui';
import { FeatureAuthDataAccessModule } from '@f1-league-v2/feature-auth/data-access';
import { EntryComponent } from './entry/entry.component';

@NgModule({
  imports: [CommonModule, FeatureAuthUiModule, FeatureAuthDataAccessModule],
  declarations: [EntryComponent],
  exports: [EntryComponent],
})
export class AuthModule {}
