import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthFacade } from '@libs/auth/data-access';

@Component({
  selector: 'f1-auth',
  template: `
    <div *ngIf="isAuthenticated$ | async; else showLoginMask">
      <f1-league-v2-auth-ui-file-upload></f1-league-v2-auth-ui-file-upload>
    </div>
    <ng-template #showLoginMask>
      <f1-league-v2-auth-ui-login></f1-league-v2-auth-ui-login>
    </ng-template>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntryComponent {
  isAuthenticated$: Observable<boolean> = this.authFacade.isAuthenticated$;

  constructor(private authFacade: AuthFacade) {}
}
