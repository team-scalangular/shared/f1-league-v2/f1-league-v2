import { Race, RaceResult } from '@generated/openapi';
import { Observable, of } from 'rxjs';
import { F1DropDownItem } from './f1-drop-down-item';

export const toDropDownItems = (racesById: Record<string, Race>): F1DropDownItem[] =>
  Object.keys(racesById).reduce(
    (dropDownItems: F1DropDownItem[], raceId: string) => [
      ...dropDownItems,
      { name: racesById[raceId].circuit, code: raceId },
    ],
    [] as F1DropDownItem[]
  );

export const excludeDrivenRacesFromDropDownItems = ([dropDownItems, raceResults]: [
  F1DropDownItem[],
  RaceResult[]
]): Observable<F1DropDownItem[]> =>
  of(
    dropDownItems.filter((dropDownItem: F1DropDownItem) =>
      raceResults.some(
        (result: RaceResult) =>
          dropDownItem.code === result.raceId && result.driverResults.length === 0
      )
    )
  );
