import { mockRacesByRaceId } from '@league-view/utils';
import { excludeDrivenRacesFromDropDownItems, toDropDownItems } from '../data-converter';
import { mockF1DropDownItems, raceResultMock } from './data-converter.mock';
import { Observable } from 'rxjs';
import { F1DropDownItem } from '../f1-drop-down-item';
import DoneCallback = jest.DoneCallback;

describe('data converter', () => {
  describe('to dropdown items', () => {
    it('should return an array with race name and id', () => {
      const mockRecord = mockRacesByRaceId;
      const expected = [
        { name: 'BAHRAIN', code: '4af87ef8-40aa-4cad-a009-65c312d6c46f' },
        { name: 'CANADA', code: '58c48a6e-4b11-418b-9220-e757fbda3289' },
      ];

      const result = toDropDownItems(mockRecord);

      expect(result).toHaveLength(2);
      expect(result).toEqual(expected);
    });
  });

  describe('excludeDrivenRacesFromDropDownItems', () => {
    let result: Observable<F1DropDownItem[]>;

    beforeEach(() => {
      result = excludeDrivenRacesFromDropDownItems([mockF1DropDownItems, raceResultMock]);
    });

    it('should return the race without any results', (done: DoneCallback) => {
      result.subscribe((res) => {
        expect(res).toHaveLength(1);
        expect(res[0].code).toEqual(raceResultMock[1].raceId);
        done();
      });
    });

    it('should exclude the driven raceResult and wrong id', (done: DoneCallback) => {
      result.subscribe((res) => {
        expect(res).not.toContain(raceResultMock[0].raceId);
        expect(res).not.toContain(raceResultMock[2].raceId);
        done();
      });
    });
  });
});
