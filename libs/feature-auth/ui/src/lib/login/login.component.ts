import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthFacade } from '@libs/auth/data-access';
import { Observable } from 'rxjs';

@Component({
  selector: 'f1-league-v2-auth-ui-login',
  template: `
    <h4 class="auth__login-heading">Login to edit this league</h4>
    <hr class="auth__separator" />
    <div class="auth__login-container">
      <form [formGroup]="loginForm" (ngSubmit)="onSubmit()">
        <div class="auth__login-input">
          <span class="p-float-label">
            <input type="text" id="user" pInputText formControlName="username" />
            <label for="user">Username</label>
          </span>
        </div>
        <div class="auth__login-input">
          <span class="p-float-label">
            <p-password
              inputId="password"
              [feedback]="false"
              formControlName="password"
            ></p-password>
            <label for="password">Password</label>
          </span>
        </div>
        <div class="auth__submit">
          <button
            pButton
            pRipple
            label="Login"
            type="submit"
            [loading]="(loading$ | async)!"
            class="p-button-secondary"
          ></button>
        </div>
      </form>
    </div>
  `,
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit {
  loading$: Observable<boolean> = this.authFacade.loading$;
  loginForm: FormGroup;

  get username(): AbstractControl {
    return <AbstractControl>this.loginForm.get('username');
  }

  get password(): AbstractControl {
    return <AbstractControl>this.loginForm.get('password');
  }

  constructor(private formBuilder: FormBuilder, private authFacade: AuthFacade) {}

  public ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: this.formBuilder.control('', Validators.required),
      password: this.formBuilder.control('', Validators.required),
    });
  }

  public onSubmit(): void {
    if (this.loginForm.valid) {
      this.authFacade.login(this.username.value, this.password.value);
    } else {
      this.authFacade.dispatchValidationError('Please provide a username and a password');
    }
  }
}
