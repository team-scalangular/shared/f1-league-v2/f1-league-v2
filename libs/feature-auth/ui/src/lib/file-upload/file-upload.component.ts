import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AuthFacade, FileUploadService } from '@libs/auth/data-access';
import { LeagueFacade } from '@libs/league-view-data-access';
import { excludeDrivenRacesFromDropDownItems, toDropDownItems } from '../utils/data-converter';
import { map, mergeMap, tap, withLatestFrom } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Maybe } from 'monet';
import { F1DropDownItem } from '../utils/f1-drop-down-item';
import { FileUpload } from 'primeng/fileupload';
import { Router } from '@angular/router';

@UntilDestroy()
@Component({
  selector: 'f1-league-v2-auth-ui-file-upload',
  template: `
    <h3>Welcome to F1 League</h3>
    <hr />
    <h3 class="my-3">Race to upload the export to:</h3>
    <div class="mb-3">
      <p-dropdown
        [options]="(racesById$ | async)!"
        optionLabel="name"
        [(ngModel)]="selectedRace"
        placeholder="Select a league for the Upload!"
        [emptyMessage]="(emptyMessage$ | async)!"
      ></p-dropdown>
    </div>
    <p-fileUpload
      #fileUpload
      class="fu__upload"
      accept=".json"
      (onSelect)="file = $event.files[0]"
      (onRemove)="file = undefined"
      [fileLimit]="1"
      [showUploadButton]="false"
      [showCancelButton]="false"
      [previewWidth]="0"
      invalidFileTypeMessageSummary="Files from type {0} are not allowed! Please provide a .json file!"
      invalidFileTypeMessageDetail=""
    >
      <ng-template pTemplate="toolbar">
        <button
          pButton
          pRipple
          [disabled]="!file || !selectedRace"
          label="Upload"
          type="button"
          (click)="dispatchFileUploadRequest()"
          class="p-button-primary d-inline-block"
        ></button>
        <div class="mt-3">Add a result file here (you can also use drag & drop!)</div>
        <div>Afterwards click upload to publish the file to the league!</div>
      </ng-template>
      <ng-template pTemplate="content">
        <div *ngIf="!file" class="text-center fu__icon-container" (click)="fileUpload.choose()">
          <i class="pi pi-cloud-upload" style="font-size: 4rem"></i>
        </div>
      </ng-template>
    </p-fileUpload>
    <div class="fu__logout">
      <button
        (click)="authFacade.logout()"
        pButton
        pRipple
        label="Logout"
        type="button"
        class="p-button-secondary"
      ></button>
    </div>
  `,
  styleUrls: ['./file-upload.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileUploadComponent implements OnInit {
  @ViewChild('fileUpload') fileUpload: FileUpload;
  file: File | undefined;
  selectedRace: F1DropDownItem | undefined;
  emptyMessage$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  racesById$: Observable<F1DropDownItem[]> = this.leagueFacade.defaultRacesById$.pipe(
    map(toDropDownItems),
    withLatestFrom(this.leagueFacade.defaultRaceResults$),
    mergeMap(excludeDrivenRacesFromDropDownItems),
    tap((dropDownItems: F1DropDownItem[]) => this.updateUIWithNew(dropDownItems))
  );

  constructor(
    private fileUploadService: FileUploadService,
    private leagueFacade: LeagueFacade,
    private router: Router,
    public authFacade: AuthFacade
  ) {}

  ngOnInit(): void {
    this.fileUploadService.fileUploadObserver$
      .pipe(untilDestroyed(this), tap(this.handleUploadSuccessInView))
      .subscribe();
  }

  private handleUploadSuccessInView(uploadSucceed: boolean): void {
    if (uploadSucceed) {
      this.file = undefined;
      this.fileUpload.clear();
    }
  }

  private updateUIWithNew(dropDownItems: F1DropDownItem[]): void {
    if (dropDownItems.length) {
      this.selectedRace = dropDownItems[0];
    }
    if (this.router.url === '/') {
      this.emptyMessage$.next('Please navigate to a league to see the filter options!');
    } else {
      this.emptyMessage$.next('There are no more races without a result in this league!');
    }
  }

  async dispatchFileUploadRequest(): Promise<void> {
    const leagueId = await this.leagueFacade.getLeagueIdAsPromise();
    Maybe.fromNull(this.selectedRace).flatMap((selectedRace: F1DropDownItem) =>
      Maybe.fromFalsy(leagueId).flatMap((leagueId: string) =>
        Maybe.fromNull(this.file).map(async (file: File) => {
          const fileText = await file.text();
          const raceId = selectedRace.code;
          this.fileUploadService.fileUploadRequest$.next({
            leagueId,
            raceId,
            fileText,
          });
        })
      )
    );
  }
}
