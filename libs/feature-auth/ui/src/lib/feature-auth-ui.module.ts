import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { ButtonModule } from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { FileUploadModule } from 'primeng/fileupload';
import { DropdownModule } from 'primeng/dropdown';
import { FeatureLeagueViewDataAccessModule } from '@libs/league-view-data-access';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    PasswordModule,
    ButtonModule,
    RippleModule,
    ReactiveFormsModule,
    FileUploadModule,
    DropdownModule,
    FeatureLeagueViewDataAccessModule,
    FormsModule,
  ],
  declarations: [LoginComponent, FileUploadComponent],
  exports: [LoginComponent, FileUploadComponent],
})
export class FeatureAuthUiModule {}
