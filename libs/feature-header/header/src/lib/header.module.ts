import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FeatureHeaderUiModule } from '@libs/header/ui';
import { EntryComponent } from './entry/entry.component';

@NgModule({
  imports: [CommonModule, FeatureHeaderUiModule],
  declarations: [EntryComponent],
  exports: [EntryComponent],
})
export class HeaderModule {}
