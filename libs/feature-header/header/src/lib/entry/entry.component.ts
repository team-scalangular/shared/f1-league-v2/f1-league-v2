import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'f1-league-v2-header-entry',
  template: `<f1-league-v2-header></f1-league-v2-header>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntryComponent {}
