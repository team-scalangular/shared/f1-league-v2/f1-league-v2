import { MenubarModule } from 'primeng/menubar';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SharedModule } from 'primeng/api';
import { MenuModule } from 'primeng/menu';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { AuthModule } from '@libs/auth';

@NgModule({
  imports: [CommonModule, MenubarModule, SharedModule, MenuModule, OverlayPanelModule, AuthModule],
  declarations: [HeaderComponent],
  exports: [HeaderComponent],
})
export class FeatureHeaderUiModule {}
