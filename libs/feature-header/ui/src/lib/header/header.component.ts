import { responsiveNavigationItems } from '@libs/utils';
import { AfterViewInit, ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { OverlayPanel } from 'primeng/overlaypanel';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { AuthFacade } from '@libs/auth/data-access';
import { skip, tap } from 'rxjs/operators';

@UntilDestroy()
@Component({
  selector: 'f1-league-v2-header',
  template: `
    <p-menubar>
      <ng-template pTemplate="start">
        <div class="header__link-box">
          <a routerLink="">
            <img src="assets/images/logo_transparent.png" height="60" alt="Logo" />
          </a>
          <h2 class="header__title">F1 League V2</h2>
        </div>
      </ng-template>
      <ng-template pTemplate="end">
        <h3
          class="header__links"
          routerLink=""
          routerLinkActive="header__links--activated"
          [routerLinkActiveOptions]="{ exact: true }"
        >
          Home
        </h3>
        <h3
          class="header__links"
          routerLink="league-view"
          routerLinkActive="header__links--activated"
        >
          View Leagues
        </h3>
        <div>
          <h3
            class="header__links"
            [ngClass]="{ 'header__links--activated': overlayPanelIsVisible }"
            (click)="togglePanel($event)"
          >
            {{ (isAuthenticated$ | async) ? 'Upload Race Result' : 'Login' }}
            <p-overlayPanel #op>
              <ng-template pTemplate>
                <f1-auth></f1-auth>
              </ng-template>
            </p-overlayPanel>
          </h3>
        </div>
        <h3 class="header__links header__links--icon" (click)="menu.toggle($event)">
          <i class="pi pi-bars"></i>
        </h3>
        <p-menu #menu [popup]="true" [model]="responsiveNavigationItems"></p-menu>
      </ng-template>
    </p-menubar>
  `,
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements AfterViewInit {
  @ViewChild('op') overlayPanel: OverlayPanel;
  responsiveNavigationItems: MenuItem[] = responsiveNavigationItems;
  overlayPanelIsVisible: boolean;

  isAuthenticated$: Observable<boolean> = this.authFacade.isAuthenticated$;

  constructor(private authFacade: AuthFacade) {}

  ngAfterViewInit(): void {
    this.overlayPanel.onHide
      .pipe(
        untilDestroyed(this),
        tap(() => (this.overlayPanelIsVisible = false))
      )
      .subscribe();
    this.isAuthenticated$
      .pipe(
        untilDestroyed(this),
        skip(1),
        tap(() => {
          this.overlayPanel.hide();
        })
      )
      .subscribe();
  }

  togglePanel(event: MouseEvent): void {
    this.overlayPanelIsVisible = true;
    this.overlayPanel.toggle(event);
  }
}
