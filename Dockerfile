# Stage 1: Build an Angular Docker Image
FROM timbru31/java-node:latest as build
ARG prodConfig=production

WORKDIR /app
COPY . ./

RUN npm ci && \
    npm run build -- --outputPath=./dist/out --configuration $prodConfig && \
    rm -rf node_modules

# Stage 2: Deploy image with nginx
FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

WORKDIR /usr/share/nginx/html
COPY --from=build /app/dist/out/ .
